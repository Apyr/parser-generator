use super::{Error, ErrorValue, ParseResult, ParseResultUtils, Position};
use std::{cell::RefCell, collections::HashMap};

#[derive(Clone, Debug)]
pub struct Cache<R: Clone>(RefCell<HashMap<u32, ParseResult<R>>>);

impl<R: Clone> Default for Cache<R> {
    fn default() -> Cache<R> {
        Cache(HashMap::new().into())
    }
}

impl<R: Clone> Cache<R> {
    pub fn cache(
        &self,
        position: Position,
        parser: impl FnOnce(Position) -> ParseResult<R>,
    ) -> ParseResult<R> {
        if let Some(result) = self.0.borrow().get(&position.offset) {
            return result.clone();
        }

        let result = parser(position);

        self.0.borrow_mut().insert(position.offset, result.clone());
        result
    }

    pub fn left_rec_cache(
        &self,
        position: Position,
        mut parser: impl FnMut(Position) -> ParseResult<R>,
    ) -> ParseResult<R> {
        if let Some(result) = self.0.borrow().get(&position.offset) {
            return result.clone();
        }

        let mut result: ParseResult<R> = Err(Error {
            position,
            value: ErrorValue::Custom("left rec default".into()),
        });
        self.0.borrow_mut().insert(position.offset, result.clone());
        loop {
            let r = parser(position);
            if r.position().offset > result.position().offset {
                result = r;
            } else {
                break;
            }
            self.0.borrow_mut().insert(position.offset, result.clone());
        }

        result
    }
}
