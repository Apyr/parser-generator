use super::{Error, ErrorValue, Input, Node, ParseResult, ParseResultUtils, Position, Span};

#[inline(always)]
pub fn node<R: Node>(
    position: Position,
    parser: impl FnOnce(Position) -> ParseResult<R>,
) -> ParseResult<R> {
    let start = position;
    parser(position).map(|(position, mut result)| {
        let span = Span {
            start,
            end: position,
        };
        result.set_span(span);
        (position, result)
    })
}

#[repr(transparent)]
pub struct Op<R: Node + Clone, F: Fn(Position) -> ParseResult<R>>(F);

impl<R, F> Op<R, F>
where
    R: Node + Clone,
    F: Fn(Position) -> ParseResult<R>,
{
    #[inline(always)]
    pub fn new(parser: F) -> Op<R, F> {
        Op(parser)
    }

    #[inline(always)]
    pub fn atom(
        self,
        parser: impl Fn(Position) -> ParseResult<R>,
    ) -> Op<R, impl Fn(Position) -> ParseResult<R>> {
        Op(move |position| parser(position).alt(|| (self.0)(position)))
    }

    #[inline(always)]
    pub fn left_assoc(
        self,
        parser: impl Fn(Position, R, &dyn Fn(Position) -> ParseResult<R>) -> ParseResult<R>,
    ) -> Op<R, impl Fn(Position) -> ParseResult<R>> {
        Op(move |position| {
            let start = position;
            let (position, mut first) = (self.0)(position)?;
            let (position, _) = many(position, |position| {
                let (position, result) = parser(position, first.clone(), &self.0)?;
                first = result;
                Ok((position, ()))
            })?;
            first.set_span(Span {
                start,
                end: position,
            });
            Ok((position, first))
        })
    }

    #[inline(always)]
    pub fn right_assoc(
        self,
        parser: impl Fn(Position, R, &dyn Fn(Position) -> ParseResult<R>) -> ParseResult<R>,
    ) -> Op<R, impl Fn(Position) -> ParseResult<R>> {
        fn call<R: Node + Clone>(
            next: &impl Fn(Position) -> ParseResult<R>,
            parser: &impl Fn(Position, R, &dyn Fn(Position) -> ParseResult<R>) -> ParseResult<R>,
            position: Position,
        ) -> ParseResult<R> {
            let start = position;
            let (position, mut first) = next(position)?;
            let (position, _) = opt(position, |position| {
                let rec = |position| call(next, parser, position);
                let (position, result) = parser(position, first.clone(), &rec)?;
                first = result;
                Ok((position, ()))
            })?;
            first.set_span(Span {
                start,
                end: position,
            });
            Ok((position, first))
        }
        Op(move |position| call(&self.0, &parser, position))
    }

    #[inline(always)]
    pub fn call(self, position: Position) -> ParseResult<R> {
        (self.0)(position)
    }
}

impl<'i> Input<'i> {
    #[inline(always)]
    pub fn as_str<R>(
        self,
        position: Position,
        parser: impl FnOnce(Position) -> ParseResult<R>,
    ) -> ParseResult<&'i str> {
        let start = position;
        parser(position).map(|(position, _)| {
            let span = Span {
                start,
                end: position,
            };
            let result = span.text(self.0);
            (position, result)
        })
    }
}

#[inline(always)]
pub fn opt<R>(
    position: Position,
    parser: impl FnOnce(Position) -> ParseResult<R>,
) -> ParseResult<Option<R>> {
    parser(position)
        .map_result(|r| Some(r))
        .or_else(|_| Ok((position, None)))
}

#[inline]
pub fn many<R>(
    mut position: Position,
    mut parser: impl FnMut(Position) -> ParseResult<R>,
) -> ParseResult<Vec<R>> {
    let mut result = vec![];
    loop {
        let r = parser(position);
        if let Ok((p, r)) = r {
            position = p;
            result.push(r);
        } else {
            break;
        }
    }
    Ok((position, result))
}

#[inline(always)]
pub fn some<R>(
    position: Position,
    mut parser: impl FnMut(Position) -> ParseResult<R>,
) -> ParseResult<Vec<R>> {
    let (position, first) = parser(position)?;
    let (position, mut rest) = many(position, parser)?;
    rest.insert(0, first);
    Ok((position, rest))
}

#[inline(always)]
pub fn and_not<R, S>(
    position: Position,
    parser: impl FnOnce(Position) -> ParseResult<R>,
    not_parser: impl FnOnce(Position) -> ParseResult<S>,
) -> ParseResult<R> {
    let result = parser(position)?;
    if not_parser(position).is_ok() {
        Err(Error {
            position,
            value: ErrorValue::ErrorExpected,
        })
    } else {
        Ok(result)
    }
}
