use super::{List, Span};
use std::fmt::Display;

#[derive(Debug, Clone, Copy)]
pub struct Config {
    pub show_skip: bool,
    pub show_span: bool,
    pub show_unnamed: bool,
}

impl Config {
    pub fn display<T: TreeDisplay>(self, value: T) -> WithConfig<T> {
        WithConfig {
            config: self,
            value,
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            show_skip: false,
            show_span: true,
            show_unnamed: true,
        }
    }
}

pub struct WithConfig<T: TreeDisplay> {
    config: Config,
    value: T,
}

impl<T: TreeDisplay> Display for WithConfig<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut fmt = Formatter {
            config: self.config,
            tabs: 0,
            was_new_line: true,
            fmt: f,
        };
        self.value.format(&mut fmt)
    }
}

pub struct Formatter<'r, 'f> {
    config: Config,
    tabs: usize,
    was_new_line: bool,
    fmt: &'r mut std::fmt::Formatter<'f>,
}

impl<'r, 'f> Formatter<'r, 'f> {
    fn write_tabs(&mut self) -> std::fmt::Result {
        for _ in 0..self.tabs - 1 {
            write!(self.fmt, "  ")?;
        }
        Ok(())
    }

    fn new_line(&mut self) -> std::fmt::Result {
        if !self.was_new_line {
            writeln!(self.fmt, "")?;
            self.was_new_line = true;
        }
        Ok(())
    }

    fn write(&mut self, value: impl Display) -> std::fmt::Result {
        self.was_new_line = false;
        write!(self.fmt, "{}", value)
    }

    pub fn start_type(&mut self, name: &str) -> std::fmt::Result {
        self.new_line()?;
        self.tabs += 1;
        self.write_tabs()?;
        self.write(name)?;
        self.write(":")?;
        self.new_line()?;
        self.tabs += 1;
        Ok(())
    }

    pub fn write_span(&mut self, span: Span) -> std::fmt::Result {
        if self.config.show_span {
            self.write_tabs()?;
            self.write("span: ")?;
            self.write(span)?;
            self.new_line()
        } else {
            Ok(())
        }
    }

    pub fn end_type(&mut self) -> std::fmt::Result {
        self.tabs -= 2;
        Ok(())
    }

    pub fn write_named(&mut self, name: &str, value: &impl TreeDisplay) -> std::fmt::Result {
        self.write_tabs()?;
        self.write(name)?;
        self.write(": ")?;
        value.format(self)?;
        self.new_line()
    }

    pub fn write_skip(&mut self, name: &str, value: &impl TreeDisplay) -> std::fmt::Result {
        if self.config.show_skip {
            self.write_named(name, value)
        } else {
            Ok(())
        }
    }

    pub fn write_unnamed(&mut self, name: &str, value: &impl TreeDisplay) -> std::fmt::Result {
        if self.config.show_unnamed {
            self.write_named(name, value)
        } else {
            Ok(())
        }
    }
}

pub trait TreeDisplay {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result;
}

impl<T: TreeDisplay> TreeDisplay for &T {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        (*self).format(fmt)
    }
}

impl<T: TreeDisplay> TreeDisplay for Box<T> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        (**self).format(fmt)
    }
}

impl TreeDisplay for bool {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.write(self)
    }
}

impl TreeDisplay for () {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.write("null")
    }
}

impl TreeDisplay for char {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.write("'")?;
        fmt.write(self.escape_default())?;
        fmt.write("'")
    }
}

impl TreeDisplay for &str {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.write('"')?;
        fmt.write(self.escape_default())?;
        fmt.write('"')
    }
}

impl<T: TreeDisplay> TreeDisplay for Vec<T> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.new_line()?;
        for item in self {
            fmt.write_tabs()?;
            fmt.write("- ")?;
            fmt.new_line()?;

            item.format(fmt)?;
            fmt.new_line()?;
        }
        Ok(())
    }
}

impl<T: TreeDisplay> TreeDisplay for Option<T> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        if let Some(value) = self {
            value.format(fmt)
        } else {
            ().format(fmt)
        }
    }
}

impl<I: TreeDisplay, S: TreeDisplay> TreeDisplay for (I, S) {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.tabs += 1;
        fmt.write_named("separator", &self.0)?;
        fmt.write_named("item", &self.1)?;
        fmt.tabs -= 1;
        Ok(())
    }
}

impl<I: TreeDisplay, S: TreeDisplay> TreeDisplay for List<I, S> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("List")?;
        fmt.write_named("first", &self.first)?;
        fmt.write_named("rest", &self.rest)?;
        fmt.write_named("trailing", &self.trailing)?;
        fmt.end_type()
    }
}
