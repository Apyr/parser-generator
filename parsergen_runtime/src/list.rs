#[derive(Clone, Debug)]
pub struct List<I, S> {
    pub first: I,
    pub rest: Vec<(S, I)>,
    pub trailing: Option<S>,
}

impl<I, S> List<I, S> {
    pub fn items(self) -> Vec<I> {
        let mut items = Vec::with_capacity(self.rest.len() + 1);
        items.push(self.first);
        items.extend(self.rest.into_iter().map(|(_, item)| item));
        items
    }

    pub fn items_ref(&self) -> Vec<&I> {
        let mut items = Vec::with_capacity(self.rest.len() + 1);
        items.push(&self.first);
        items.extend(self.rest.iter().map(|(_, item)| item));
        items
    }
}
