use std::fmt::Display;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Position {
    pub offset: u32,
    pub line: u32,
    pub column: u32,
}

impl Position {
    pub fn line_part<'i>(&self, text: &'i str) -> &'i str {
        let text = &text[self.offset as usize..];
        let end = text.find('\n').unwrap_or(text.len());
        &text[..end]
    }
}

impl PartialOrd for Position {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.offset.partial_cmp(&other.offset)
    }
}

impl Ord for Position {
    #[inline]
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.offset.cmp(&other.offset)
    }
}

impl Default for Position {
    #[inline]
    fn default() -> Self {
        Self {
            offset: 0,
            line: 1,
            column: 1,
        }
    }
}

impl Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}", self.line, self.column)
    }
}

impl Position {
    #[inline]
    pub fn inc(&mut self, ch: char) {
        self.offset += ch.len_utf8() as u32;
        if ch != '\n' {
            self.column += 1;
        } else {
            self.column = 1;
            self.line += 1;
        }
    }

    #[inline]
    pub fn next(mut self, ch: char) -> Position {
        self.inc(ch);
        self
    }
}
