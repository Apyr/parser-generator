use crate::{Error, ErrorValue, Errors, Position};

pub type ParseResult<R> = Result<(Position, R), Error>;

// utils

pub trait ParseResultUtils {
    type Result;
    fn unit(self) -> ParseResult<()>;
    fn rule(self, name: &'static str) -> Self;
    fn cast<T>(self) -> ParseResult<T>
    where
        T: From<Self::Result>;
    fn alt<F>(self, parser: F) -> Self
    where
        F: FnOnce() -> Self;
    fn map_result<T, F>(self, func: F) -> ParseResult<T>
    where
        F: FnOnce(Self::Result) -> T;
    fn position(&self) -> Position;
    fn catch(self, errors: &Errors) -> Self;
}

impl<R> ParseResultUtils for ParseResult<R> {
    type Result = R;

    #[inline(always)]
    fn unit(self) -> ParseResult<()> {
        self.map_result(|_| ())
    }

    #[inline(always)]
    fn rule(self, name: &'static str) -> Self {
        self.map_err(|mut error| {
            error.value.at_rule(name);
            error
        })
    }

    #[inline(always)]
    fn cast<T>(self) -> ParseResult<T>
    where
        T: From<Self::Result>,
    {
        self.map_result(|result| T::from(result))
    }

    #[inline(always)]
    fn alt<F>(self, parser: F) -> Self
    where
        F: FnOnce() -> Self,
    {
        self.or_else(|error1| {
            parser().map_err(|error2| {
                if error1.position.offset >= error2.position.offset {
                    error1
                } else {
                    error2
                }
            })
        })
    }

    #[inline(always)]
    fn map_result<T, F>(self, func: F) -> ParseResult<T>
    where
        F: FnOnce(Self::Result) -> T,
    {
        self.map(|(stream, result)| (stream, func(result)))
    }

    #[inline(always)]
    fn position(&self) -> Position {
        match self {
            Ok((position, _)) => *position,
            Err(err) => err.position,
        }
    }

    #[inline(always)]
    fn catch(self, errors: &Errors) -> Self {
        match self {
            r @ Ok(_) => r,
            Err(mut error) => {
                errors.set_error(&mut error);
                Err(error)
            }
        }
    }
}
