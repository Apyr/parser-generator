use super::Position;
use std::{
    cell::{Cell, RefCell},
    collections::HashSet,
    fmt::{Debug, Display},
};

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Error {
    pub position: Position,
    pub value: ErrorValue,
}

#[derive(Debug, Clone, Default)]
pub struct ErrorSet(pub HashSet<ErrorValue>);

impl ErrorSet {
    pub fn take(&mut self, value: &mut ErrorValue) {
        if let ErrorValue::Set(set) = value {
            self.0.extend(std::mem::take(&mut set.0));
        } else {
            self.0.insert(value.clone());
        }
    }

    pub fn add(&mut self, value: &ErrorValue) {
        if let ErrorValue::Set(set) = value {
            self.0.extend(set.0.clone());
        } else {
            self.0.insert(value.clone());
        }
    }

    pub fn at_rule(&mut self, name: &'static str) {
        self.0 = self
            .0
            .drain()
            .map(|mut v| {
                v.at_rule(name);
                v
            })
            .collect();
    }
}

impl PartialEq for ErrorSet {
    fn eq(&self, other: &Self) -> bool {
        false
    }
}

impl Eq for ErrorSet {}

impl std::hash::Hash for ErrorSet {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        state.write_u8(0);
    }
}

impl Error {
    fn append(&mut self, error: &Error) {
        let mut set = ErrorSet::default();
        set.take(&mut self.value);
        set.add(&error.value);
        self.value = ErrorValue::Set(set);
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ErrorValue {
    Custom(String),
    Set(ErrorSet),
    AtRule(&'static str, Box<ErrorValue>),
    UnexpectedEOF,
    ExpectedEOF(char),
    UnexpectedChar { expected: char, got: char },
    UnexpectedCharRange { start: char, end: char, got: char },
    UnexpectedStr(&'static str),
    ErrorExpected,
}

impl ErrorValue {
    pub fn at_rule(&mut self, name: &'static str) {
        if let ErrorValue::Set(set) = self {
            set.at_rule(name);
        } else if !matches!(self, ErrorValue::AtRule(_, _)) {
            *self = ErrorValue::AtRule(name, self.clone().into());
        }
    }

    fn is_rule(&self) -> bool {
        matches!(self, ErrorValue::AtRule(_, _))
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.position != Position::default() {
            write!(f, "{}: ", self.position)?;
        }
        write!(f, "{}", self.value)
    }
}

impl Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl Display for ErrorValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use ErrorValue::*;
        match *self {
            Custom(ref msg) => write!(f, "{}", msg),
            Set(ref errors) => {
                write!(f, "{{")?;
                for err in errors.0.iter() {
                    write!(f, "{}; ", err)?;
                }
                write!(f, "}}")?;
                Ok(())
            }
            AtRule(name, ref error) => write!(f, "<{}> {}", name, error),
            UnexpectedEOF => write!(f, "unexpected EOF"),
            ExpectedEOF(c) => write!(f, "expected EOF, got '{}'", c),
            UnexpectedChar { expected, got } => write!(f, "expected '{}', got '{}'", expected, got),
            UnexpectedCharRange { start, end, got } => {
                write!(f, "expected '{}'..'{}', got '{}'", start, end, got)
            }
            UnexpectedStr(s) => write!(f, "expected \"{}\"", s),
            ErrorExpected => write!(f, "error expected"),
        }
    }
}

impl std::error::Error for Error {}

#[derive(Debug, Clone)]
pub struct Errors {
    last_error: RefCell<Option<Error>>,
    is_stoped: Cell<bool>,
}

impl Errors {
    pub fn new() -> Errors {
        Errors {
            last_error: None.into(),
            is_stoped: Cell::new(false),
        }
    }

    pub fn stop(&self) {
        self.is_stoped.set(true);
    }

    pub fn start(&self) {
        self.is_stoped.set(false);
    }

    pub fn set_error(&self, error: &mut Error) {
        if self.is_stoped.get() {
            return;
        }

        let mut last_error = self.last_error.borrow_mut();
        if let Some(last) = last_error.as_mut() {
            if error.position.offset > last.position.offset {
                *last_error = Some(error.clone());
            } else if last.position.offset == error.position.offset && error.value.is_rule() {
                last.append(error);
                *error = last.clone();
            } else {
                *error = last.clone();
            }
        } else {
            *last_error = Some(error.clone());
        }
    }
}
