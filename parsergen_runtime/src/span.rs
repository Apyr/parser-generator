use super::Position;
use std::fmt::{Debug, Display};

#[derive(Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Span {
    pub start: Position,
    pub end: Position,
}

impl Span {
    pub fn text<'i>(&self, text: &'i str) -> &'i str {
        &text[self.start.offset as usize..self.end.offset as usize]
    }
}

impl Display for Span {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-{}", self.start, self.end)
    }
}

impl Debug for Span {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\"{}\"", self)
    }
}

pub trait Node {
    fn get_span(&self) -> Span;
    fn set_span(&mut self, span: Span);
}
