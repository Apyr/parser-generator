use super::{Error, ErrorValue, ParseResult, Position};
use std::fmt::Debug;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Input<'i>(pub &'i str);

// parsers

impl<'i> Input<'i> {
    #[inline]
    pub fn next(self, position: Position) -> ParseResult<char> {
        let ch = self.0[position.offset as usize..]
            .chars()
            .next()
            .ok_or_else(|| Error {
                position,
                value: ErrorValue::UnexpectedEOF,
            })?;
        Ok((position.next(ch), ch))
    }

    #[inline]
    pub fn parse_char(self, position: Position, c: char) -> ParseResult<char> {
        let start = position;
        let (position, ch) = self.next(position)?;
        if ch == c {
            Ok((position, c))
        } else {
            Err(Error {
                position: start,
                value: ErrorValue::UnexpectedChar {
                    expected: c,
                    got: ch,
                },
            })
        }
    }

    #[inline]
    pub fn parse_char_range(self, position: Position, start: char, end: char) -> ParseResult<char> {
        let start_pos = position;
        let (position, ch) = self.next(position)?;
        if start <= ch && ch <= end {
            Ok((position, ch))
        } else {
            Err(Error {
                position: start_pos,
                value: ErrorValue::UnexpectedCharRange {
                    start,
                    end,
                    got: ch,
                },
            })
        }
    }

    #[inline]
    pub fn parse_str(self, mut position: Position, s: &'static str) -> ParseResult<&'static str> {
        let start = position;
        for c in s.chars() {
            position = self
                .parse_char(position, c)
                .map_err(|_| Error {
                    position: start,
                    value: ErrorValue::UnexpectedStr(s),
                })?
                .0;
        }
        Ok((position, s))
    }

    #[inline]
    pub fn parse_eof(self, position: Position) -> ParseResult<()> {
        let start = position;
        match self.next(position) {
            Ok((_, c)) => Err(Error {
                position: start,
                value: ErrorValue::ExpectedEOF(c),
            }),
            Err(_) => Ok((position, ())),
        }
    }
}
