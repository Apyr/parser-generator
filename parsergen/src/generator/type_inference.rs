use super::{reachable::get_reachable, Expr, ExprValue, Rules, Type, TypeDef, TypeDefKind};
use convert_case::{Case, Casing};
use indexmap::{IndexMap, IndexSet};
use std::{
    collections::{HashMap, HashSet},
    mem::{swap, take},
};

impl Rules {
    pub(super) fn infer_types(&mut self) {
        let mut inf = Inference::new();
        inf.infer(self);
        self.typedefs = inf.typedefs;

        let mut inf = LifetimeInference::new();
        inf.infer(&mut self.typedefs);

        let mut inf = BoxInference::new(take(&mut self.typedefs));
        inf.infer();
        self.typedefs = inf.typedefs;
    }
}

struct Inference {
    typedefs: IndexMap<String, TypeDef>,
    first_call: bool,
    ops_self: String,
}

impl Inference {
    fn new() -> Inference {
        Inference {
            typedefs: IndexMap::new(),
            first_call: true,
            ops_self: "".into(),
        }
    }

    fn infer(&mut self, rules: &mut Rules) {
        self.typedefs.reserve(rules.rules.len());

        for rule in rules.rules.values_mut() {
            self.first_call = true;
            self.infer_expr(&rule.name.to_case(Case::UpperCamel), &mut rule.body);
        }
    }

    fn infer_expr(&mut self, type_name: &str, expr: &mut Expr) {
        self._infer_expr(type_name, false, expr);
    }

    fn _infer_expr(&mut self, type_name: &str, freeze_type_name: bool, expr: &mut Expr) {
        use ExprValue::*;
        let first_call = self.first_call;
        self.first_call = false;
        let ty = match &mut expr.value {
            Rule(name) => Type::TypeName(name.to_case(Case::UpperCamel)),
            OpsSelf(_) => Type::TypeName(self.ops_self.clone()),
            Char(_) | CharRange(_, _) | ANY => Type::Char,
            Str(_) => Type::Str,
            EOF => Type::Unit,
            Named(_, expr) => {
                self.infer_expr(type_name, expr);
                expr.ty.clone()
            }
            NamedPart(name, expr) => {
                let type_name = name.to_case(Case::UpperCamel);
                self._infer_expr(&type_name, true, expr);
                expr.ty.clone()
            }
            Seq(exprs) => {
                let mut type_name = if first_call || freeze_type_name {
                    type_name.to_string()
                } else {
                    format!("{}Seq", type_name)
                };
                {
                    let initial = type_name.clone();
                    let mut i = 0;
                    while self.typedefs.contains_key(&type_name) {
                        type_name = initial.clone() + &i.to_string();
                        i += 1;
                    }
                }

                let mut fields = IndexMap::with_capacity(exprs.len());
                let mut i = 0;
                for expr in exprs.iter_mut() {
                    let name = match &expr.value {
                        Named(name, _) => name.clone(),
                        _ => {
                            let name = format!("_{}", i);
                            i += 1;
                            name
                        }
                    };
                    self.infer_expr(&type_name, expr);
                    fields.insert(name, expr.ty.clone());
                }

                self.typedefs.insert(
                    type_name.clone(),
                    TypeDef {
                        name: type_name.clone(),
                        fields,
                        kind: TypeDefKind::Struct,
                        has_lifetime: false,
                    },
                );

                Type::TypeName(type_name)
            }
            Alt(exprs, is_ops) => {
                let mut type_name = if freeze_type_name {
                    type_name.to_string()
                } else {
                    format!("{}Alt", type_name)
                };
                {
                    let initial = type_name.clone();
                    let mut i = 0;
                    while self.typedefs.contains_key(&type_name) {
                        type_name = initial.clone() + &i.to_string();
                        i += 1;
                    }
                }

                let old = if *is_ops {
                    let old = self.ops_self.clone();
                    self.ops_self = type_name.clone();
                    Option::Some(old)
                } else {
                    None
                };

                let mut types = IndexSet::with_capacity(exprs.len());
                let mut type_map = HashMap::new();
                for expr in exprs.iter_mut() {
                    self.infer_expr(&type_name, expr);
                    self.dedup_type(expr, &mut type_map);
                    types.insert(expr.ty.clone());
                }

                if let Option::Some(old) = old {
                    self.ops_self = old;
                }

                if types.len() == 1 {
                    types.into_iter().next().unwrap()
                } else {
                    let mut fields = IndexMap::with_capacity(types.len());
                    for ty in types {
                        fields.insert(ty.variant_name(), ty);
                    }

                    self.typedefs.insert(
                        type_name.clone(),
                        TypeDef {
                            name: type_name.clone(),
                            fields,
                            kind: TypeDefKind::Enum,
                            has_lifetime: false,
                        },
                    );

                    Type::TypeName(type_name)
                }
            }
            Opt(expr) => {
                self.infer_expr(type_name, expr);
                let ty = expr.ty.clone();
                Type::Opt(ty.into())
            }
            Many(expr) | Some(expr) => {
                self.infer_expr(type_name, expr);
                let ty = expr.ty.clone();
                Type::Vec(ty.into())
            }
            AsStr(expr) => {
                self.fill_str(expr);
                Type::Str
            }
            AndNot(expr, not) => {
                self.infer_expr(type_name, expr);
                self.infer_expr(type_name, not);
                expr.ty.clone()
            }
            List { item, sep, .. } => {
                let item_name = format!("{}Item", type_name);
                let sep_name = format!("{}Sep", type_name);
                self.infer_expr(&item_name, item);
                self.infer_expr(&sep_name, sep);
                Type::List(item.ty.clone().into(), sep.ty.clone().into())
            }
        };
        expr.ty = ty;
    }

    fn fill_str(&mut self, expr: &mut Expr) {
        use ExprValue::*;
        expr.ty = Type::Str;
        match &mut expr.value {
            Rule(_) | OpsSelf(_) | Char(_) | CharRange(_, _) | Str(_) | EOF | ANY => {}
            Named(_, expr)
            | NamedPart(_, expr)
            | Opt(expr)
            | Many(expr)
            | Some(expr)
            | AsStr(expr) => self.fill_str(expr),
            Seq(exprs) | Alt(exprs, _) => {
                for expr in exprs {
                    self.fill_str(expr);
                }
            }
            AndNot(expr1, expr2)
            | List {
                item: expr1,
                sep: expr2,
                ..
            } => {
                self.fill_str(expr1);
                self.fill_str(expr2);
            }
        }
    }

    fn dedup_type(&mut self, expr: &mut Expr, type_map: &mut HashMap<Vec<(String, Type)>, String>) {
        if let Type::TypeName(name) = &expr.ty {
            if let Some(typedef) = self.typedefs.get(name) {
                if typedef.kind != TypeDefKind::Struct {
                    return;
                }
                let fields: Vec<_> = typedef.fields.clone().into_iter().collect();
                if let Some(old_name) = type_map.get(&fields) {
                    self.typedefs.remove(name);
                    expr.ty = Type::TypeName(old_name.clone());
                } else {
                    type_map.insert(fields, name.clone());
                }
            }
        }
    }
}

struct LifetimeInference {
    visited: HashSet<String>,
    typedefs: IndexMap<String, TypeDef>,
}

impl LifetimeInference {
    fn new() -> LifetimeInference {
        LifetimeInference {
            visited: HashSet::new(),
            typedefs: IndexMap::new(),
        }
    }

    fn infer(&mut self, typedefs: &mut IndexMap<String, TypeDef>) {
        self.typedefs = take(typedefs);
        self.visited.reserve(self.typedefs.len());

        let names: Vec<_> = self.typedefs.keys().cloned().collect();
        for name in names.iter() {
            self.infer_typedef(name);
        }

        self.visited.clear();
        for name in names.iter() {
            self.infer_typedef(name);
        }

        swap(&mut self.typedefs, typedefs);
    }

    fn infer_typedef(&mut self, name: &str) -> bool {
        if self.visited.contains(name) {
            self.typedefs.get(name).unwrap().has_lifetime
        } else {
            self.visited.insert(name.to_string());
            let mut result = false;
            let fields: Vec<_> = self
                .typedefs
                .get(name)
                .unwrap()
                .fields
                .values()
                .cloned()
                .collect();
            for ty in fields {
                if self.infer_type(&ty) {
                    result = true;
                    break;
                }
            }
            self.typedefs.get_mut(name).unwrap().has_lifetime = result;
            result
        }
    }

    fn infer_type(&mut self, ty: &Type) -> bool {
        use Type::*;
        match ty {
            Unknown | Unit | Char => false,
            Str => true,
            TypeName(name) => self.infer_typedef(name),
            Opt(ty) | Vec(ty) | Box(ty) => self.infer_type(ty),
            List(i, s) => {
                let i = self.infer_type(i);
                let s = self.infer_type(s);
                i || s
            }
        }
    }
}

struct BoxInference {
    typedefs: IndexMap<String, TypeDef>,
    reachable: HashMap<String, HashSet<String>>,
}

impl BoxInference {
    fn new(typedefs: IndexMap<String, TypeDef>) -> BoxInference {
        let len = typedefs.len();
        BoxInference {
            typedefs,
            reachable: HashMap::with_capacity(len),
        }
    }

    fn infer(&mut self) {
        let typedefs = self
            .typedefs
            .values()
            .filter(|t| t.kind == TypeDefKind::Struct)
            .cloned()
            .collect::<Vec<_>>();
        for mut typedef in typedefs {
            self.infer_typedef(&mut typedef);
            self.typedefs.insert(typedef.name.clone(), typedef);
        }
    }

    fn reachable(&mut self, name: &str) -> &HashSet<String> {
        if !self.reachable.contains_key(name) {
            let set = get_reachable(&self.typedefs, name);
            self.reachable.insert(name.to_string(), set);
        }
        self.reachable.get(name).unwrap()
    }

    fn infer_typedef(&mut self, typedef: &mut TypeDef) {
        for ty in typedef.fields.values_mut() {
            self.infer_type(&typedef.name, ty);
        }
    }

    fn infer_type(&mut self, type_name: &str, type_: &mut Type) {
        use Type::*;
        match type_ {
            TypeName(name) => {
                if name == type_name || self.reachable(name).contains(type_name) {
                    self.reachable.remove(name);
                    *type_ = Box(TypeName(name.clone()).into());
                }
            }
            Opt(ty) => self.infer_type(type_name, ty),
            List(i, s) => {
                self.infer_type(type_name, i);
                self.infer_type(type_name, s);
            }
            Unknown | Unit | Char | Str | Vec(_) | Box(_) => {}
        }
    }
}
