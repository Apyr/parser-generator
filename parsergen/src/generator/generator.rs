use super::{Expr, ExprValue, Rule, Rules, Type, TypeDef, TypeDefKind};

impl Rules {
    pub fn generate(&self) -> String {
        CodeGen::new(self).build()
    }
}

impl ExprValue {
    fn is_term(&self) -> bool {
        use ExprValue::*;
        match self {
            Rule(_) | OpsSelf(_) | Char(_) | CharRange(_, _) | Str(_) | EOF | ANY => true,
            Named(_, _)
            | NamedPart(_, _)
            | Seq(_)
            | Alt(_, _)
            | Opt(_)
            | Many(_)
            | Some(_)
            | AsStr(_)
            | AndNot(_, _)
            | List { .. } => false,
        }
    }
}
impl Expr {
    fn is_ops_self(&self) -> Option<bool> {
        match &self.value {
            ExprValue::Named(_, e) | ExprValue::NamedPart(_, e) => e.is_ops_self(),
            ExprValue::OpsSelf(v) => Some(*v),
            _ => None,
        }
    }

    fn get_first(&self) -> (&Expr, &[Expr]) {
        match &self.value {
            ExprValue::Seq(exprs) => {
                let first = &exprs[0];
                (first, exprs)
            }
            ExprValue::NamedPart(_, e) => e.get_first(),
            _ => (self, &[]),
        }
    }
}

struct CodeGen<'r> {
    rules: &'r Rules,
    current_rule: Option<&'r Rule>,
    code: Vec<String>,
    caches: Vec<(String, String)>,
    tabs: usize,
    need_skip: bool,
}

impl<'r> CodeGen<'r> {
    fn new(context: &'r Rules) -> Self {
        CodeGen {
            rules: context,
            current_rule: None,
            code: vec![],
            caches: vec![],
            tabs: 0,
            need_skip: false,
        }
    }

    fn add(&mut self, s: impl Into<String>) {
        let s: String = s.into();
        let mut line = String::with_capacity(s.len() + self.tabs * 4);
        for _ in 0..self.tabs {
            line += "    ";
        }
        line += &s;
        self.code.push(line);
    }

    fn begin(&mut self, s: impl Into<String>) {
        self.add(s);
        self.tabs += 1;
    }

    fn end(&mut self, s: impl Into<String>) {
        self.tabs -= 1;
        self.add(s);
    }

    fn build(mut self) -> String {
        self.add("use parsergen_runtime::*;");
        self.add("");

        self.add("// types");
        self.add("");
        for typedef in self.rules.typedefs.values() {
            self.gen_typedef(typedef);
            self.add("");
        }

        self.add("// parser");

        self.add(
            r"
#[allow(dead_code)]
#[derive(Clone, Debug)]
pub struct Parser<'i> {
    position: Position,
    input: Input<'i>,
    errors: Errors,
    cache: Caches<'i>,
}

impl<'i> Parser<'i> {
    pub fn new(text: &'i str) -> Self {
        Parser {
            position: Position::default(),
            input: Input(text),
            errors: Errors::new(),
            cache: Caches::default(),
        }
    }
}",
        );
        self.add("");

        self.add("#[allow(dead_code)]");
        self.begin("impl<'i> Parser<'i> {");

        for rule in self.rules.rules.values() {
            self.gen_rule(rule);
            self.add("");
        }

        self.end("}");
        self.add("");

        self.add("// cache");
        self.add("#[derive(Clone, Debug, Default)]");
        self.begin("struct Caches<'i> {");
        for (name, ty) in self.caches.clone() {
            self.add(format!("{}_: Cache<{}>,", name, ty));
        }
        self.add("_d: std::marker::PhantomData<&'i ()>,");
        self.end("}");

        self.add("// impls");
        self.add("");
        for typedef in self.rules.typedefs.values() {
            self.gen_typedef_impl(typedef);
        }

        self.code.join("\n")
    }

    fn gen_typedef(&mut self, typedef: &TypeDef) {
        let lifetime = if typedef.has_lifetime { "<'i>" } else { "" };
        self.add("#[derive(Clone, Debug)]");
        match typedef.kind {
            TypeDefKind::Struct => {
                self.begin(format!("pub struct {}{} {{", typedef.name, lifetime));
                for (name, ty) in typedef.fields.iter() {
                    let ty = self.gen_type(ty);
                    self.add(format!("pub {}: {},", name, ty));
                }
                self.add("pub span: Span,");
                self.end("}");
            }
            TypeDefKind::Enum => {
                self.begin(format!("pub enum {}{} {{", typedef.name, lifetime,));
                for (name, ty) in typedef.fields.iter() {
                    let ty = self.gen_type(ty);
                    self.add(format!("{}({}),", name, ty));
                }
                self.end("}");
            }
        }
    }

    fn gen_typedef_impl(&mut self, typedef: &TypeDef) {
        let lifetime = if typedef.has_lifetime { "<'i>" } else { "" };
        match typedef.kind {
            TypeDefKind::Struct => {
                self.begin(format!(
                    "impl{} Node for {}{} {{",
                    lifetime, typedef.name, lifetime
                ));
                self.begin("fn set_span(&mut self, span: Span) {");
                self.add("self.span = span;");
                self.end("}");
                self.begin("fn get_span(&self) -> Span {");
                self.add("self.span");
                self.end("}");
                self.end("}");

                self.begin(format!(
                    "impl{} TreeDisplay for {}{} {{",
                    lifetime, typedef.name, lifetime
                ));
                self.begin("fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {");
                self.add(format!("fmt.start_type(\"{}\")?;", typedef.name));
                for name in typedef.fields.keys() {
                    if name.starts_with("_skip") {
                        self.add(format!("fmt.write_skip(\"{}\", &self.{})?;", name, name));
                    } else if name.starts_with("_") {
                        self.add(format!("fmt.write_unnamed(\"{}\", &self.{})?;", name, name));
                    } else {
                        self.add(format!("fmt.write_named(\"{}\", &self.{})?;", name, name));
                    }
                }
                self.add("fmt.write_span(self.span)?;");
                self.add("fmt.end_type()");
                self.end("}");
                self.end("}");
            }
            TypeDefKind::Enum => {
                for (name, ty) in typedef.fields.iter() {
                    let ty = self.gen_type(ty);
                    self.begin(format!(
                        "impl{} From<{}> for {}{} {{",
                        lifetime, ty, typedef.name, lifetime
                    ));
                    self.begin(format!("fn from(v: {}) -> Self {{", ty));
                    self.add(format!("Self::{}(v)", name));
                    self.end("}");
                    self.end("}")
                }

                self.begin(format!(
                    "impl{} Node for {}{} {{",
                    lifetime, typedef.name, lifetime
                ));

                self.begin("fn set_span(&mut self, span: Span) {");
                self.begin("match self {");
                for (name, ty) in typedef.fields.iter() {
                    if ty.type_name().is_some() {
                        self.add(format!("Self::{}(value) => value.set_span(span),", name));
                    } else {
                        self.add(format!("Self::{}(_) => {{}}", name));
                    }
                }
                self.end("}");
                self.end("}");

                self.begin("fn get_span(&self) -> Span {");
                self.begin("match self {");
                for (name, ty) in typedef.fields.iter() {
                    if ty.type_name().is_some() {
                        self.add(format!("Self::{}(value) => value.get_span(),", name));
                    } else {
                        self.add(format!("Self::{}(_) => Span::default(),", name));
                    }
                }
                self.end("}");
                self.end("}");

                self.end("}");

                self.begin(format!(
                    "impl{} TreeDisplay for {}{} {{",
                    lifetime, typedef.name, lifetime
                ));
                self.begin("fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {");
                self.begin("match self {");
                for name in typedef.fields.keys() {
                    self.add(format!("Self::{}(value) => value.format(fmt),", name));
                }
                self.end("}");
                self.end("}");
                self.end("}");
            }
        }
    }

    fn gen_type(&self, ty: &Type) -> String {
        use Type::*;
        match ty {
            Unknown => unreachable!(),
            Unit => "()".into(),
            Char => "char".into(),
            Str => "&'i str".into(),
            TypeName(name) => {
                let lifetime = if self
                    .rules
                    .typedefs
                    .get(name)
                    .expect(&format!("'{}' not found", name))
                    .has_lifetime
                {
                    "<'i>"
                } else {
                    ""
                };
                format!("{}{}", name, lifetime)
            }
            Opt(ty) => format!("Option<{}>", self.gen_type(ty)),
            Vec(ty) => format!("Vec<{}>", self.gen_type(ty)),
            Box(ty) => format!("Box<{}>", self.gen_type(ty)),
            List(i, s) => format!("List<{}, {}>", self.gen_type(i), self.gen_type(s)),
        }
    }

    fn gen_rule(&mut self, rule: &'r Rule) {
        self.current_rule = Some(rule);
        self.need_skip =
            !self.rules.save_skips && rule.need_skip() && self.rules.rules.contains_key("skip");

        let ty = self.gen_type(&rule.body.ty);
        self.begin(format!(
            "pub fn parse_{}(&mut self) -> Result<{}, Error> {{",
            rule.name, ty
        ));
        self.add("let position = self.position;");
        self.add(format!(
            "let (position, result) = self._parse_{}(position)?;",
            rule.name
        ));
        self.add("self.position = position;");
        self.add("Ok(result)");
        self.end("}");

        self.begin(format!(
            "fn _parse_{}(&self, position: Position) -> ParseResult<{}> {{",
            rule.name, ty
        ));
        if rule.name == "skip" {
            self.add("self.errors.stop();");
        }
        if rule.is_cache {
            self.caches.push((rule.name.clone(), ty.clone()));
            self.begin(format!(
                "self.cache.{}_.cache(position, |position| {{",
                rule.name,
            ));
        }
        let result = self.gen_expr(&rule.body, false);
        self.add(format!("let rule_result = {};", result));
        if rule.name == "skip" {
            self.add("self.errors.start();");
        }
        self.add("rule_result");
        if rule.is_cache {
            self.end("})");
        }
        self.end("}");
    }

    fn gen_seq(&mut self, exprs: &[Expr], ty: &Type, is_op: bool) -> String {
        let type_name = ty.unbox().type_name().unwrap();
        let fields = &self.rules.typedefs.get(type_name).unwrap().fields;

        self.begin("let seq_result = node(position, |position| {");

        let mut args = Vec::with_capacity(exprs.len());
        for (i, expr) in exprs.iter().enumerate() {
            args.push(format!("{}: v{}.into()", fields.get_index(i).unwrap().0, i));
            if is_op && i == 0 {
                continue;
            }
            let expr = self.gen_expr(expr, false);
            self.add(format!("let (position, v{}) = {}?;", i, expr));
        }

        self.add(format!(
            "let value = {} {{ {}, span: Span::default(), }};",
            type_name,
            args.join(", ")
        ));
        self.add("Ok((position, value))");

        self.end("});");
        "seq_result".into()
    }

    fn gen_seq_str(&mut self, exprs: &[Expr]) -> String {
        self.begin("let seq_result = (|| {");
        for expr in exprs.iter() {
            let expr = self.gen_expr(expr, true);
            self.add(format!("let (position, _) = {}?;", expr));
        }
        self.add("Ok((position, ()))");
        self.end("})();");
        "seq_result".into()
    }

    fn gen_expr(&mut self, expr: &Expr, is_str: bool) -> String {
        self.add(format!("// {}", expr));
        if self.need_skip && expr.value.is_term() {
            self.add("let (position, _) = self._parse_skip(position)?;");
        }
        use ExprValue::*;
        let mut result = match &expr.value {
            Rule(name) => format!("self._parse_{}(position)", name),
            OpsSelf(_) => "parse(position)".into(),
            Char(c) => format!("self.input.parse_char(position, '{}')", c.escape_default()),
            CharRange(start, end) => {
                assert!(start < end);
                format!(
                    "self.input.parse_char_range(position, '{}', '{}')",
                    start.escape_default(),
                    end.escape_default()
                )
            }
            Str(s) => format!("self.input.parse_str(position, \"{}\")", s.escape_default()),
            EOF => "self.input.parse_eof(position)".into(),
            ANY => "self.input.next(position)".into(),
            Named(_, expr) | NamedPart(_, expr) => self.gen_expr(expr, is_str),
            Seq(exprs) => {
                assert_ne!(exprs.len(), 0);
                if is_str {
                    self.gen_seq_str(exprs)
                } else {
                    self.gen_seq(exprs, &expr.ty, false)
                }
            }
            Alt(exprs, false) => {
                assert_ne!(exprs.len(), 0);
                let type_name = expr.ty.unbox().type_name();
                self.begin("let alt_result = ");
                for (i, expr) in exprs.iter().enumerate() {
                    if i == 0 {
                        self.begin("(|| {");
                    } else {
                        self.begin(".alt(|| {");
                    }

                    let expr = self.gen_expr(expr, is_str);
                    if is_str {
                        self.add(format!("{}.unit()", expr));
                    } else {
                        if let Option::Some(type_name) = type_name {
                            self.add(format!("{}.cast::<{}>()", expr, type_name));
                        } else {
                            self.add(expr);
                        }
                    }

                    if i == 0 {
                        self.end("})()");
                    } else {
                        self.end("})");
                    }
                }
                self.end(";");
                "alt_result".into()
            }
            Alt(exprs, true) => {
                self.begin("let ops_result = ");
                let type_name = expr.ty.unbox().type_name().unwrap();
                for (i, expr) in exprs.iter().rev().enumerate() {
                    let (first, all) = expr.get_first();
                    if let Option::Some(is_right_assoc) = first.is_ops_self() {
                        if is_right_assoc {
                            self.begin(".right_assoc(|position, v0, parse| {");
                        } else {
                            self.begin(".left_assoc(|position, v0, parse| {");
                        }
                        let expr = self.gen_seq(all, &expr.ty, true);
                        self.add(format!("{}.cast::<{}>()", expr, type_name));
                    } else {
                        if i == 0 {
                            self.begin("Op::new(|position| {");
                        } else {
                            self.begin(".atom(|position| {")
                        }
                        let expr = self.gen_expr(expr, is_str);
                        self.add(format!("{}.cast::<{}>()", expr, type_name));
                    }
                    self.end("})");
                }
                self.end(".call(position);");
                "ops_result".into()
            }
            Opt(expr) => {
                self.begin("let opt_result = opt(position, |position| {");
                let expr = self.gen_expr(expr, is_str);
                self.add(expr);
                self.end("});");
                "opt_result".into()
            }
            Many(expr) => {
                self.begin("let many_result = many(position, |position| {");
                let expr = self.gen_expr(expr, is_str);
                self.add(expr);
                self.end("});");
                "many_result".into()
            }
            Some(expr) => {
                self.begin("let some_result = some(position, |position| {");
                let expr = self.gen_expr(expr, is_str);
                self.add(expr);
                self.end("});");
                "some_result".into()
            }
            AsStr(expr) => {
                self.begin("let as_str_result = self.input.as_str(position, |position| {");
                let expr = self.gen_expr(expr, true);
                self.add(expr);
                self.end("});");
                "as_str_result".into()
            }
            AndNot(expr, not) => {
                self.begin("let and_not_result = and_not(position, |position| {");
                let expr = self.gen_expr(expr, is_str);
                self.add(expr);
                self.add("}, |position| {");
                let not = self.gen_expr(not, is_str);
                self.add(not);
                self.end("});");
                "and_not_result".into()
            }
            List {
                item,
                sep,
                is_trailing,
            } => {
                self.begin("let list_result = (|| {");

                let item_gen = self.gen_expr(item, is_str);
                self.add(format!("let (position, first) = {}?;", item_gen));

                self.begin("let (position, rest) = many(position, |position| {");
                let sep_gen = self.gen_expr(sep, is_str);
                self.add(format!("let (position, sep) = {}?;", sep_gen));
                let item_gen = self.gen_expr(item, is_str);
                self.add(format!("let (position, item) = {}?;", item_gen));
                self.add("Ok((position, (sep, item)))");
                self.end("})?;");

                if *is_trailing {
                    self.begin("let (position, trailing) = opt(position, |position| {");
                    let sep_gen = self.gen_expr(sep, is_str);
                    self.add(sep_gen);
                    self.end("})?;");
                } else {
                    self.add("let trailing = None;");
                }

                self.add("Ok((position, List { first, rest, trailing }))");
                self.end("})();");
                "list_result".into()
            }
        };

        if expr.value.is_term() {
            result = format!(
                "{}.rule(\"{}\").catch(&self.errors)",
                result,
                self.current_rule.unwrap().name,
            );
        }
        result
    }
}
