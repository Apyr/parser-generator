use super::{Type, TypeDef};
use indexmap::IndexMap;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Rules {
    pub rules: IndexMap<String, Rule>,
    pub typedefs: IndexMap<String, TypeDef>,
    pub save_skips: bool,
}

impl Rules {
    pub fn new() -> Rules {
        Rules {
            rules: IndexMap::new(),
            typedefs: IndexMap::new(),
            save_skips: false,
        }
    }

    pub fn push(&mut self, rule: Rule) {
        self.rules.insert(rule.name.clone(), rule);
    }

    pub fn add(&mut self, name: impl Into<String>, body: impl Into<Expr>) -> &mut Rule {
        self.push(Rule::new(name, body));
        self.rules.last_mut().unwrap().1
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Rule {
    pub name: String,
    pub body: Expr,
    pub is_atomic: bool,
    pub is_cache: bool,
}

impl Rule {
    pub fn new(name: impl Into<String>, body: impl Into<Expr>) -> Rule {
        Rule {
            name: name.into(),
            body: body.into(),
            is_atomic: false,
            is_cache: false,
        }
    }

    pub(super) fn need_skip(&self) -> bool {
        !self.is_atomic && self.name != "skip"
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
pub struct Expr {
    pub value: ExprValue,
    pub ty: Type,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ExprValue {
    Rule(String),
    OpsSelf(bool /* is_right_assoc */),
    Char(char),
    CharRange(char, char),
    Str(String),
    EOF,
    ANY,
    Named(String, Box<Expr>),
    NamedPart(String, Box<Expr>),
    Seq(Vec<Expr>),
    Alt(Vec<Expr>, bool /* is_ops */),
    Opt(Box<Expr>),
    Many(Box<Expr>),
    Some(Box<Expr>),
    AsStr(Box<Expr>),
    AndNot(Box<Expr>, Box<Expr>),
    List {
        item: Box<Expr>,
        sep: Box<Expr>,
        is_trailing: bool,
    },
}

impl Default for ExprValue {
    fn default() -> Self {
        Self::EOF
    }
}

impl From<ExprValue> for Expr {
    fn from(value: ExprValue) -> Self {
        Expr {
            value,
            ty: Type::default(),
        }
    }
}

impl From<ExprValue> for Box<Expr> {
    fn from(value: ExprValue) -> Self {
        Box::new(value.into())
    }
}
