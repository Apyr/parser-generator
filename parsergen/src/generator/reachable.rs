use super::{Type, TypeDef};
use indexmap::IndexMap;
use std::collections::HashSet;

pub(super) fn get_reachable(typedefs: &IndexMap<String, TypeDef>, name: &str) -> HashSet<String> {
    let mut result = HashSet::new();
    _get_reachable(typedefs, name, &mut result);
    result
}

fn _get_reachable(typedefs: &IndexMap<String, TypeDef>, name: &str, result: &mut HashSet<String>) {
    if let Some(typedef) = typedefs.get(name) {
        result.insert(name.to_string());
        for ty in typedef.fields.values() {
            _get_reachable_type(typedefs, ty, result)
        }
    }
}

fn _get_reachable_type(
    typedefs: &IndexMap<String, TypeDef>,
    ty: &Type,
    result: &mut HashSet<String>,
) {
    use Type::*;
    match ty {
        TypeName(name) => {
            let contains = !result.contains(name);
            result.insert(name.clone());
            if contains {
                _get_reachable(typedefs, name, result);
            }
        }
        Opt(ty) => _get_reachable_type(typedefs, ty, result),
        List(i, s) => {
            _get_reachable_type(typedefs, i, result);
            _get_reachable_type(typedefs, s, result);
        }
        Unknown | Unit | Char | Str | Vec(_) | Box(_) => {}
    }
}
