use indexmap::IndexMap;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Type {
    Unknown,
    Unit,
    Char,
    Str,
    TypeName(String),
    Opt(Box<Type>),
    Vec(Box<Type>),
    Box(Box<Type>),
    List(Box<Type>, Box<Type>),
}

impl Default for Type {
    fn default() -> Self {
        Type::Unknown
    }
}

impl Type {
    pub fn variant_name(&self) -> String {
        use Type::*;
        match self {
            Unknown => unreachable!(),
            Unit => "Unit".into(),
            Char => "Char".into(),
            Str => "Str".into(),
            TypeName(name) => name.clone(),
            Opt(ty) => format!("Opt{}", ty.variant_name()),
            Vec(ty) => format!("Vec{}", ty.variant_name()),
            Box(ty) => ty.variant_name(),
            List(i, s) => format!("List{}{}", i.variant_name(), s.variant_name()),
        }
    }

    pub fn type_name(&self) -> Option<&str> {
        if let Type::TypeName(name) = self {
            Some(name)
        } else {
            None
        }
    }

    pub fn unbox(&self) -> &Type {
        if let Type::Box(ty) = self {
            ty
        } else {
            self
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TypeDefKind {
    Struct,
    Enum,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TypeDef {
    pub name: String,
    pub fields: IndexMap<String, Type>,
    pub kind: TypeDefKind,
    pub has_lifetime: bool,
}
