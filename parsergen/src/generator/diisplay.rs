use super::{Expr, ExprValue, Rule, Rules};
use std::fmt::{Display, Formatter};

impl Display for ExprValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use ExprValue::*;
        match self {
            Rule(name) => write!(f, "{}", name),
            OpsSelf(is_right_assoc) => {
                if *is_right_assoc {
                    write!(f, "@right")
                } else {
                    write!(f, "@self")
                }
            }
            Char(c) => write!(f, "'{}'", c.escape_default()),
            CharRange(start, end) => write!(
                f,
                "'{}'..'{}'",
                start.escape_default(),
                end.escape_default()
            ),
            Str(s) => write!(f, "\"{}\"", s.escape_default()),
            EOF => write!(f, "EOF"),
            ANY => write!(f, "ANY"),
            Named(name, expr) => write!(f, "{}={}", name, expr),
            NamedPart(name, expr) => write!(f, "{}@{}", name, expr),
            Seq(exprs) => {
                write!(f, "(")?;
                for (i, expr) in exprs.iter().enumerate() {
                    if i != 0 {
                        write!(f, " ")?;
                    }
                    write!(f, "{}", expr)?;
                }
                write!(f, ")")
            }
            Alt(exprs, is_ops) => {
                if *is_ops {
                    write!(f, "@ops")?;
                }
                write!(f, "(")?;
                for (i, expr) in exprs.iter().enumerate() {
                    if i != 0 {
                        write!(f, " | ")?;
                    }
                    write!(f, "{}", expr)?;
                }
                write!(f, ")")
            }
            Opt(expr) => write!(f, "{}?", expr),
            Many(expr) => write!(f, "{}*", expr),
            Some(expr) => write!(f, "{}+", expr),
            AsStr(expr) => write!(f, "$({})", expr),
            AndNot(expr, not) => write!(f, "{} ~ {}", expr, not),
            List {
                item,
                sep,
                is_trailing,
            } => {
                if *is_trailing {
                    write!(f, "{} ,+ {}", item, sep)
                } else {
                    write!(f, "{} ,* {}", item, sep)
                }
            }
        }
    }
}

impl Display for Expr {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl Display for Rule {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: {};", self.name, self.body)
    }
}

impl Display for Rules {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for rule in self.rules.values() {
            writeln!(f, "{}", rule)?;
        }
        Ok(())
    }
}
