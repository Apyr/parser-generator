use super::{ExprValue, Rules};
use std::{collections::HashSet, mem::take};

impl Rules {
    pub fn prepare(&mut self) -> Result<(), String> {
        let mut errors = Vec::new();
        for rule in self.rules.values() {
            self.check(&rule.name, &rule.body.value, &mut errors);
        }
        if !errors.is_empty() {
            return Err(errors.join("\n"));
        }

        let mut with_skip = HashSet::new();
        for rule in self.rules.values() {
            if rule.need_skip() {
                with_skip.insert(rule.name.clone());
            }
        }

        let need_skip = self.save_skips && self.rules.contains_key("skip");
        for rule in self.rules.values_mut() {
            rule.body.value.wrap_seq();
            rule.body
                .value
                .fill_seq_skip(true, need_skip && rule.need_skip(), &with_skip);
            rule.body.value.simplify(false, true);
        }

        self.infer_types();

        Ok(())
    }

    fn check(&self, rule_name: &str, expr: &ExprValue, errors: &mut Vec<String>) {
        use ExprValue::*;
        match expr {
            Rule(name) => {
                if !self.rules.contains_key(name) {
                    errors.push(format!(
                        "error in rule '{}': '{}' not found",
                        rule_name, name
                    ));
                }
            }
            OpsSelf(_) | Char(_) | CharRange(_, _) | Str(_) | EOF | ANY => {}
            Seq(exprs) | Alt(exprs, _) => {
                for expr in exprs {
                    self.check(rule_name, &expr.value, errors);
                }
            }
            Named(_, expr)
            | NamedPart(_, expr)
            | Opt(expr)
            | Many(expr)
            | Some(expr)
            | AsStr(expr) => {
                self.check(rule_name, &expr.value, errors);
            }
            AndNot(expr1, expr2)
            | List {
                item: expr1,
                sep: expr2,
                ..
            } => {
                self.check(rule_name, &expr1.value, errors);
                self.check(rule_name, &expr2.value, errors);
            }
        }
    }
}

impl ExprValue {
    fn fill_seq_skip(&mut self, first_skip: bool, need_skip: bool, with_skip: &HashSet<String>) {
        use ExprValue::*;
        fn skip(val: &mut ExprValue) {
            *val = Seq(vec![Rule("skip".into()).into(), take(val).into()])
        }

        match self {
            Rule(name) => {
                if need_skip && !with_skip.contains(name) {
                    skip(self);
                }
            }
            OpsSelf(_) | Char(_) | CharRange(_, _) | Str(_) | EOF | ANY => {
                if need_skip {
                    skip(self);
                }
            }
            Named(_, expr) => {
                expr.value.fill_seq_skip(false, need_skip, with_skip);
                if need_skip {
                    skip(self);
                }
                self.wrap_seq();
            }
            Seq(exprs) => {
                for (i, expr) in exprs.iter_mut().enumerate() {
                    expr.value
                        .fill_seq_skip(!first_skip && i == 0, need_skip, with_skip);
                }
            }
            Alt(exprs, _) => {
                for expr in exprs {
                    expr.value.fill_seq_skip(first_skip, need_skip, with_skip);
                }
            }
            Opt(expr) => {
                expr.value.fill_seq_skip(false, need_skip, with_skip);
                if need_skip {
                    skip(self);
                }
            }
            NamedPart(_, expr) | Many(expr) | Some(expr) | AsStr(expr) => {
                expr.value.fill_seq_skip(true, need_skip, with_skip);
            }
            AndNot(expr1, expr2)
            | List {
                item: expr1,
                sep: expr2,
                ..
            } => {
                expr1.value.fill_seq_skip(true, need_skip, with_skip);
                expr2.value.fill_seq_skip(true, need_skip, with_skip);
            }
        }
    }

    fn wrap_seq(&mut self) {
        if !matches!(self, ExprValue::Seq(_)) {
            *self = ExprValue::Seq(vec![take(self).into()]).into();
        }
    }

    fn simplify(&mut self, remove_seq: bool, top_level_seq: bool) {
        use ExprValue::*;
        match self {
            Rule(_) | OpsSelf(_) | Char(_) | CharRange(_, _) | Str(_) | EOF | ANY => {}
            Named(_, expr)
            | NamedPart(_, expr)
            | Opt(expr)
            | Many(expr)
            | Some(expr)
            | AsStr(expr) => expr.value.simplify(true, top_level_seq),
            Seq(exprs) => {
                let mut new_exprs = vec![];
                for mut expr in exprs.drain(0..) {
                    expr.value.simplify(true, false);
                    if let Seq(exprs) = expr.value {
                        new_exprs.extend(exprs);
                    } else {
                        new_exprs.push(expr);
                    }
                }
                *exprs = new_exprs;

                let mut skip_count = 0;
                let mut unnamed_count = 0;
                for expr in exprs.iter_mut() {
                    if expr.value.is_skip() {
                        *expr = Named(
                            format!("_skip{}", skip_count),
                            take(expr).value.unname().into(),
                        )
                        .into();
                        skip_count += 1;
                    } else if !matches!(&expr.value, Named(_, _)) {
                        unnamed_count += 1;
                    }
                }

                if remove_seq && exprs.len() == 1 {
                    if !matches!(exprs[0].value, Named(_, _)) {
                        *self = exprs.pop().unwrap().value;
                    }
                } else {
                    // set name to value
                    if top_level_seq && unnamed_count == 1 && exprs.len() - skip_count == 1 {
                        for expr in exprs {
                            if !expr.value.is_skip() {
                                let name = if matches!(expr.value, AsStr(_)) {
                                    "str"
                                } else {
                                    "value"
                                };
                                expr.value = Named(name.into(), take(expr).into());
                            }
                        }
                    }
                }
            }
            Alt(exprs, _) => {
                for expr in exprs {
                    expr.value.simplify(true, true);
                }
            }
            AndNot(expr1, expr2)
            | List {
                item: expr1,
                sep: expr2,
                ..
            } => {
                expr1.value.simplify(true, true);
                expr2.value.simplify(true, true);
            }
        }
    }

    fn is_skip(&self) -> bool {
        if let ExprValue::Rule(name) = self {
            name == "skip"
        } else if let ExprValue::Named(_, expr) = self {
            expr.value.is_skip()
        } else {
            false
        }
    }

    pub(super) fn unname(self) -> Self {
        if let ExprValue::Named(_, e) = self {
            e.value.unname()
        } else {
            self
        }
    }
}
