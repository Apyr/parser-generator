use crate::rules_parser::compile_rules;
use std::{
    env::current_dir,
    ffi::OsStr,
    fs::{read_to_string, write},
    path::Path,
};

fn generate(grammar_file: &Path, output_file: &Path) -> std::io::Result<()> {
    let text = read_to_string(grammar_file)?;

    let rules = compile_rules(&text).unwrap();
    let code = rules.generate();

    write(output_file, code)
}

fn walk_dir(path: &Path, ext: &str) -> std::io::Result<()> {
    if path.is_dir() {
        for dir in std::fs::read_dir(path)? {
            let dir = dir?.path();
            if dir.is_dir() {
                walk_dir(&dir, ext)?;
            } else if dir.is_file() && dir.extension() == Some(OsStr::new(ext)) {
                let output = dir.with_extension("rs");
                generate(&dir, &output)?;
            }
        }
    }
    Ok(())
}

pub fn build(ext: Option<&str>) -> std::io::Result<()> {
    let ext = ext.unwrap_or("peg");
    let dir = current_dir()?;
    walk_dir(&dir, ext)
}
