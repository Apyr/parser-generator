use super::parser as p;
use crate::{Expr, ExprValue, Rule, Rules};
use parsergen_runtime::{Error, ErrorValue, Span};
use std::mem::take;

impl Expr {
    fn get_named_part(&self) -> Option<String> {
        match &self.value {
            ExprValue::Named(_, e) | ExprValue::AsStr(e) => e.get_named_part(),
            ExprValue::NamedPart(name, _) => Some(name.clone()),
            _ => None,
        }
    }

    fn as_ops(&mut self, span: Span) -> Result<(), String> {
        if let ExprValue::Alt(_, is_ops) = &mut self.value {
            *is_ops = true;
            Ok(())
        } else {
            Err(format!("{}: expected an alternative in @ops", span))
        }
    }
}

pub fn compile_rules(text: &str) -> Result<Rules, Error> {
    let decls = p::Parser::new(text).parse_decls()?;
    let rules = visit(decls).map_err(|e| Error {
        position: Default::default(),
        value: ErrorValue::Custom(e),
    })?;
    Ok(rules)
}

fn visit(decls: p::Decls) -> Result<Rules, String> {
    let mut result = Rules::new();
    for decl in decls.decls {
        match decl.value {
            p::DeclAlt::Rule(rule) => {
                let name = rule.name.str;
                let body = visit_alt(rule.body)?;

                let rule_ref = result.add(name, body);
                if let Some(modifiers) = rule.modifiers {
                    for m in modifiers.modifiers.items() {
                        visit_modif(rule_ref, m)?;
                    }
                }
            }
            p::DeclAlt::RulesOption(_) => {
                result.save_skips = true;
            }
        }
    }

    result.prepare()?;
    Ok(result)
}

fn visit_modif(rule_ref: &mut Rule, m: p::Modifier) -> Result<(), String> {
    match m.str {
        "str" => {
            rule_ref.is_atomic = true;
            rule_ref.body = ExprValue::AsStr(take(&mut rule_ref.body).into()).into();
        }
        "atomic" => {
            rule_ref.is_atomic = true;
        }
        "cached" => {
            rule_ref.is_cache = true;
        }
        "operators" => {
            rule_ref.body.as_ops(m.span)?;
        }
        _ => unreachable!(),
    }
    Ok(())
}

fn visit_alt(expr: p::Alternative) -> Result<Expr, String> {
    let mut exprs = vec![];
    exprs.push(visit_seq(expr.first)?);
    for e in expr.rest {
        exprs.push(visit_seq(e.seq)?);
    }
    let result = if exprs.len() == 1 {
        exprs.pop().unwrap()
    } else {
        ExprValue::Alt(exprs, false).into()
    };
    Ok(result)
}

fn visit_seq(expr: p::Sequence) -> Result<Expr, String> {
    let mut exprs = Vec::with_capacity(expr.value.len()); //: Vec<_> = expr.value.into_iter().map(visit_binop).collect();
    let mut named_part = None;
    for expr in expr.value {
        let expr = visit_named(expr)?;
        if let Some(name) = expr.get_named_part() {
            named_part = Some(name);
        } else {
            exprs.push(expr);
        }
    }
    let result = if exprs.len() == 1 {
        exprs.pop().unwrap()
    } else {
        ExprValue::Seq(exprs).into()
    };
    let result = if let Some(name) = named_part {
        ExprValue::NamedPart(name, result.into()).into()
    } else {
        result
    };
    Ok(result)
}

fn visit_named(expr: p::Named) -> Result<Expr, String> {
    let atom = expr.value.atom;
    let mut result: Expr = match atom.value {
        p::AtomAlt::Eof(_) => ExprValue::EOF.into(),
        p::AtomAlt::Any(_) => ExprValue::ANY.into(),
        p::AtomAlt::Ident(name) => ExprValue::Rule(name.str.into()).into(),
        p::AtomAlt::CharRange(range) => {
            ExprValue::CharRange(visit_char(range.start), visit_char(range.end)).into()
        }
        p::AtomAlt::Char(c) => ExprValue::Char(visit_char(c)).into(),
        p::AtomAlt::Str(s) => ExprValue::Str(visit_str(s)).into(),
        p::AtomAlt::SubExpr(e) => visit_alt(e.value)?,
        p::AtomAlt::AsStr(e) => ExprValue::AsStr(visit_alt(e.value)?.into()).into(),
        p::AtomAlt::OpsSelf(_) => ExprValue::OpsSelf(false).into(),
        p::AtomAlt::OpsRight(_) => ExprValue::OpsSelf(true).into(),
        p::AtomAlt::NamedPart(name) => {
            ExprValue::NamedPart(name.name.str.into(), Expr::default().into()).into()
        }
        p::AtomAlt::ListExpr(list) => ExprValue::List {
            item: visit_alt(list.item)?.into(),
            sep: visit_alt(list.sep)?.into(),
            is_trailing: list.trailing.is_some(),
        }
        .into(),
        p::AtomAlt::Ops(ops) => {
            let mut expr = visit_alt(ops.value)?;
            expr.as_ops(ops.span)?;
            expr
        }
    };

    for op in expr.value.ops {
        match op {
            p::PostfixAlt::Char('?') => result = ExprValue::Opt(result.into()).into(),
            p::PostfixAlt::Char('*') => result = ExprValue::Many(result.into()).into(),
            p::PostfixAlt::Char('+') => result = ExprValue::Some(result.into()).into(),
            p::PostfixAlt::Char(_) => unreachable!(),
            p::PostfixAlt::AndNot(e) => {
                result = ExprValue::AndNot(result.into(), visit_alt(e.not)?.into()).into()
            }
        }
    }

    let result = if let Some(name) = expr.name {
        ExprValue::Named(name.name.str.into(), result.into()).into()
    } else {
        result
    };
    Ok(result)
}

fn visit_char(expr: p::Char) -> char {
    match expr.char {
        p::CharAlt::Escape(e) => visit_escape(e),
        p::CharAlt::CharEscape(_) => '\'',
        p::CharAlt::CharChar(c) => c.value,
    }
}

fn visit_str(expr: p::Str) -> String {
    let mut result = String::with_capacity(expr.chars.len());
    for val in expr.chars {
        let c = match val {
            p::StrAlt::Escape(e) => visit_escape(e),
            p::StrAlt::StrEscape(_) => '"',
            p::StrAlt::StrChar(c) => c.value,
        };
        result.push(c);
    }
    result
}

fn visit_escape(e: p::Escape) -> char {
    match e.value {
        'n' => '\n',
        'r' => '\r',
        't' => '\t',
        '\\' => '\\',
        _ => unreachable!(),
    }
}
