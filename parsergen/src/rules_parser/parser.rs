use parsergen_runtime::*;

// types

#[derive(Clone, Debug)]
pub struct Decls<'i> {
    pub decls: Vec<Decl<'i>>,
    pub _0: (),
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum DeclAlt<'i> {
    Rule(Rule<'i>),
    RulesOption(RulesOption<'i>),
}

#[derive(Clone, Debug)]
pub struct Decl<'i> {
    pub value: DeclAlt<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct RulesOption<'i> {
    pub _0: &'i str,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct RuleSeq<'i> {
    pub _0: char,
    pub modifiers: List<Modifier<'i>, char>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Rule<'i> {
    pub name: Ident<'i>,
    pub modifiers: Option<RuleSeq<'i>>,
    pub _0: char,
    pub body: Alternative<'i>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Modifier<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct AlternativeSeq<'i> {
    pub _0: char,
    pub seq: Sequence<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Alternative<'i> {
    pub _0: Option<char>,
    pub first: Sequence<'i>,
    pub rest: Vec<AlternativeSeq<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Sequence<'i> {
    pub value: Vec<Named<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct NamedSeq<'i> {
    pub name: Ident<'i>,
    pub _0: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Named<'i> {
    pub name: Option<NamedSeq<'i>>,
    pub value: Postfix<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum PostfixAlt<'i> {
    Char(char),
    AndNot(AndNot<'i>),
}

#[derive(Clone, Debug)]
pub struct Postfix<'i> {
    pub atom: Atom<'i>,
    pub ops: Vec<PostfixAlt<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct AndNot<'i> {
    pub _0: char,
    pub not: Alternative<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum AtomAlt<'i> {
    Eof(Eof<'i>),
    Any(Any<'i>),
    Ident(Ident<'i>),
    CharRange(CharRange<'i>),
    Char(Char<'i>),
    Str(Str<'i>),
    SubExpr(SubExpr<'i>),
    AsStr(AsStr<'i>),
    NamedPart(NamedPart<'i>),
    ListExpr(ListExpr<'i>),
    Ops(Ops<'i>),
    OpsSelf(OpsSelf<'i>),
    OpsRight(OpsRight<'i>),
}

#[derive(Clone, Debug)]
pub struct Atom<'i> {
    pub value: AtomAlt<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Eof<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Any<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Ident<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct CharRange<'i> {
    pub start: Char<'i>,
    pub _0: &'i str,
    pub end: Char<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum CharAlt<'i> {
    Escape(Escape),
    CharEscape(CharEscape<'i>),
    CharChar(CharChar),
}

#[derive(Clone, Debug)]
pub struct Char<'i> {
    pub _0: char,
    pub char: CharAlt<'i>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum StrAlt<'i> {
    Escape(Escape),
    StrEscape(StrEscape<'i>),
    StrChar(StrChar),
}

#[derive(Clone, Debug)]
pub struct Str<'i> {
    pub _0: char,
    pub chars: Vec<StrAlt<'i>>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct SubExpr<'i> {
    pub _0: char,
    pub value: Alternative<'i>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct AsStr<'i> {
    pub _0: &'i str,
    pub _1: char,
    pub value: Alternative<'i>,
    pub _2: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct NamedPart<'i> {
    pub _0: &'i str,
    pub _1: char,
    pub name: Ident<'i>,
    pub _2: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ListExprSeq<'i> {
    pub _0: char,
    pub _1: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ListExpr<'i> {
    pub _0: &'i str,
    pub _1: char,
    pub item: Alternative<'i>,
    pub _2: char,
    pub sep: Alternative<'i>,
    pub trailing: Option<ListExprSeq<'i>>,
    pub _3: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Ops<'i> {
    pub _0: &'i str,
    pub _1: char,
    pub value: Alternative<'i>,
    pub _2: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct OpsSelf<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct OpsRight<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Escape {
    pub _0: char,
    pub value: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct CharChar {
    pub value: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct CharEscape<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct StrChar {
    pub value: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct StrEscape<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum SkipAlt<'i> {
    Ws(Ws<'i>),
    SingleLineComment(SingleLineComment<'i>),
    MultiLineComment(MultiLineComment<'i>),
}

#[derive(Clone, Debug)]
pub struct Skip<'i> {
    pub value: Vec<SkipAlt<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct SingleLineComment<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct MultiLineComment<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Ws<'i> {
    pub str: &'i str,
    pub span: Span,
}

// parser

#[allow(dead_code)]
#[derive(Clone, Debug)]
pub struct Parser<'i> {
    position: Position,
    input: Input<'i>,
    errors: Errors,
    cache: Caches<'i>,
}

impl<'i> Parser<'i> {
    pub fn new(text: &'i str) -> Self {
        Parser {
            position: Position::default(),
            input: Input(text),
            errors: Errors::new(),
            cache: Caches::default(),
        }
    }
}

#[allow(dead_code)]
impl<'i> Parser<'i> {
    pub fn parse_decls(&mut self) -> Result<Decls<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_decls(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_decls(&self, position: Position) -> ParseResult<Decls<'i>> {
        // (decls=decl* EOF)
        let seq_result = node(position, |position| {
            // decls=decl*
            // decl*
            let many_result = many(position, |position| {
                // decl
                let (position, _) = self._parse_skip(position)?;
                self._parse_decl(position).rule("decls").catch(&self.errors)
            });
            let (position, v0) = many_result?;
            // EOF
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_eof(position).rule("decls").catch(&self.errors)?;
            let value = Decls { decls: v0.into(), _0: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_decl(&mut self) -> Result<Decl<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_decl(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_decl(&self, position: Position) -> ParseResult<Decl<'i>> {
        // (value=(rule | rules_option))
        let seq_result = node(position, |position| {
            // value=(rule | rules_option)
            // (rule | rules_option)
            let alt_result = 
                (|| {
                    // rule
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_rule(position).rule("decl").catch(&self.errors).cast::<DeclAlt>()
                })()
                .alt(|| {
                    // rules_option
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_rules_option(position).rule("decl").catch(&self.errors).cast::<DeclAlt>()
                })
            ;
            let (position, v0) = alt_result?;
            let value = Decl { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_rules_option(&mut self) -> Result<RulesOption<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_rules_option(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_rules_option(&self, position: Position) -> ParseResult<RulesOption<'i>> {
        // ("@@save_skips" ';')
        let seq_result = node(position, |position| {
            // "@@save_skips"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "@@save_skips").rule("rules_option").catch(&self.errors)?;
            // ';'
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_char(position, ';').rule("rules_option").catch(&self.errors)?;
            let value = RulesOption { _0: v0.into(), _1: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_rule(&mut self) -> Result<Rule<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_rule(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_rule(&self, position: Position) -> ParseResult<Rule<'i>> {
        // (name=ident modifiers=('(' modifiers=modifier ,* ',' ')')? ':' body=alternative ';')
        let seq_result = node(position, |position| {
            // name=ident
            // ident
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_ident(position).rule("rule").catch(&self.errors)?;
            // modifiers=('(' modifiers=modifier ,* ',' ')')?
            // ('(' modifiers=modifier ,* ',' ')')?
            let opt_result = opt(position, |position| {
                // ('(' modifiers=modifier ,* ',' ')')
                let seq_result = node(position, |position| {
                    // '('
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v0) = self.input.parse_char(position, '(').rule("rule").catch(&self.errors)?;
                    // modifiers=modifier ,* ','
                    // modifier ,* ','
                    let list_result = (|| {
                        // modifier
                        let (position, _) = self._parse_skip(position)?;
                        let (position, first) = self._parse_modifier(position).rule("rule").catch(&self.errors)?;
                        let (position, rest) = many(position, |position| {
                            // ','
                            let (position, _) = self._parse_skip(position)?;
                            let (position, sep) = self.input.parse_char(position, ',').rule("rule").catch(&self.errors)?;
                            // modifier
                            let (position, _) = self._parse_skip(position)?;
                            let (position, item) = self._parse_modifier(position).rule("rule").catch(&self.errors)?;
                            Ok((position, (sep, item)))
                        })?;
                        let trailing = None;
                        Ok((position, List { first, rest, trailing }))
                    })();
                    let (position, v1) = list_result?;
                    // ')'
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v2) = self.input.parse_char(position, ')').rule("rule").catch(&self.errors)?;
                    let value = RuleSeq { _0: v0.into(), modifiers: v1.into(), _1: v2.into(), span: Span::default(), };
                    Ok((position, value))
                });
                seq_result
            });
            let (position, v1) = opt_result?;
            // ':'
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, ':').rule("rule").catch(&self.errors)?;
            // body=alternative
            // alternative
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self._parse_alternative(position).rule("rule").catch(&self.errors)?;
            // ';'
            let (position, _) = self._parse_skip(position)?;
            let (position, v4) = self.input.parse_char(position, ';').rule("rule").catch(&self.errors)?;
            let value = Rule { name: v0.into(), modifiers: v1.into(), _0: v2.into(), body: v3.into(), _1: v4.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_modifier(&mut self) -> Result<Modifier<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_modifier(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_modifier(&self, position: Position) -> ParseResult<Modifier<'i>> {
        // (str=$(("str" | "atomic" | "cached" | "operators")))
        let seq_result = node(position, |position| {
            // str=$(("str" | "atomic" | "cached" | "operators"))
            // $(("str" | "atomic" | "cached" | "operators"))
            let as_str_result = self.input.as_str(position, |position| {
                // ("str" | "atomic" | "cached" | "operators")
                let alt_result = 
                    (|| {
                        // "str"
                        self.input.parse_str(position, "str").rule("modifier").catch(&self.errors).unit()
                    })()
                    .alt(|| {
                        // "atomic"
                        self.input.parse_str(position, "atomic").rule("modifier").catch(&self.errors).unit()
                    })
                    .alt(|| {
                        // "cached"
                        self.input.parse_str(position, "cached").rule("modifier").catch(&self.errors).unit()
                    })
                    .alt(|| {
                        // "operators"
                        self.input.parse_str(position, "operators").rule("modifier").catch(&self.errors).unit()
                    })
                ;
                alt_result
            });
            let (position, v0) = as_str_result?;
            let value = Modifier { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_alternative(&mut self) -> Result<Alternative<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_alternative(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_alternative(&self, position: Position) -> ParseResult<Alternative<'i>> {
        // ('|'? first=sequence rest=('|' seq=sequence)*)
        let seq_result = node(position, |position| {
            // '|'?
            let opt_result = opt(position, |position| {
                // '|'
                let (position, _) = self._parse_skip(position)?;
                self.input.parse_char(position, '|').rule("alternative").catch(&self.errors)
            });
            let (position, v0) = opt_result?;
            // first=sequence
            // sequence
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_sequence(position).rule("alternative").catch(&self.errors)?;
            // rest=('|' seq=sequence)*
            // ('|' seq=sequence)*
            let many_result = many(position, |position| {
                // ('|' seq=sequence)
                let seq_result = node(position, |position| {
                    // '|'
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v0) = self.input.parse_char(position, '|').rule("alternative").catch(&self.errors)?;
                    // seq=sequence
                    // sequence
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v1) = self._parse_sequence(position).rule("alternative").catch(&self.errors)?;
                    let value = AlternativeSeq { _0: v0.into(), seq: v1.into(), span: Span::default(), };
                    Ok((position, value))
                });
                seq_result
            });
            let (position, v2) = many_result?;
            let value = Alternative { _0: v0.into(), first: v1.into(), rest: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_sequence(&mut self) -> Result<Sequence<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_sequence(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_sequence(&self, position: Position) -> ParseResult<Sequence<'i>> {
        // (value=named+)
        let seq_result = node(position, |position| {
            // value=named+
            // named+
            let some_result = some(position, |position| {
                // named
                let (position, _) = self._parse_skip(position)?;
                self._parse_named(position).rule("sequence").catch(&self.errors)
            });
            let (position, v0) = some_result?;
            let value = Sequence { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_named(&mut self) -> Result<Named<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_named(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_named(&self, position: Position) -> ParseResult<Named<'i>> {
        // (name=(name=ident '=')? value=postfix)
        let seq_result = node(position, |position| {
            // name=(name=ident '=')?
            // (name=ident '=')?
            let opt_result = opt(position, |position| {
                // (name=ident '=')
                let seq_result = node(position, |position| {
                    // name=ident
                    // ident
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v0) = self._parse_ident(position).rule("named").catch(&self.errors)?;
                    // '='
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v1) = self.input.parse_char(position, '=').rule("named").catch(&self.errors)?;
                    let value = NamedSeq { name: v0.into(), _0: v1.into(), span: Span::default(), };
                    Ok((position, value))
                });
                seq_result
            });
            let (position, v0) = opt_result?;
            // value=postfix
            // postfix
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_postfix(position).rule("named").catch(&self.errors)?;
            let value = Named { name: v0.into(), value: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_postfix(&mut self) -> Result<Postfix<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_postfix(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_postfix(&self, position: Position) -> ParseResult<Postfix<'i>> {
        // (atom=atom ops=('*' | '+' | '?' | and_not)*)
        let seq_result = node(position, |position| {
            // atom=atom
            // atom
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_atom(position).rule("postfix").catch(&self.errors)?;
            // ops=('*' | '+' | '?' | and_not)*
            // ('*' | '+' | '?' | and_not)*
            let many_result = many(position, |position| {
                // ('*' | '+' | '?' | and_not)
                let alt_result = 
                    (|| {
                        // '*'
                        let (position, _) = self._parse_skip(position)?;
                        self.input.parse_char(position, '*').rule("postfix").catch(&self.errors).cast::<PostfixAlt>()
                    })()
                    .alt(|| {
                        // '+'
                        let (position, _) = self._parse_skip(position)?;
                        self.input.parse_char(position, '+').rule("postfix").catch(&self.errors).cast::<PostfixAlt>()
                    })
                    .alt(|| {
                        // '?'
                        let (position, _) = self._parse_skip(position)?;
                        self.input.parse_char(position, '?').rule("postfix").catch(&self.errors).cast::<PostfixAlt>()
                    })
                    .alt(|| {
                        // and_not
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_and_not(position).rule("postfix").catch(&self.errors).cast::<PostfixAlt>()
                    })
                ;
                alt_result
            });
            let (position, v1) = many_result?;
            let value = Postfix { atom: v0.into(), ops: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_and_not(&mut self) -> Result<AndNot<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_and_not(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_and_not(&self, position: Position) -> ParseResult<AndNot<'i>> {
        // ('~' not=alternative)
        let seq_result = node(position, |position| {
            // '~'
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '~').rule("and_not").catch(&self.errors)?;
            // not=alternative
            // alternative
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_alternative(position).rule("and_not").catch(&self.errors)?;
            let value = AndNot { _0: v0.into(), not: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_atom(&mut self) -> Result<Atom<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_atom(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_atom(&self, position: Position) -> ParseResult<Atom<'i>> {
        // (value=(eof | any | ident | char_range | char | str | sub_expr | as_str | named_part | list_expr | ops | ops_self | ops_right))
        let seq_result = node(position, |position| {
            // value=(eof | any | ident | char_range | char | str | sub_expr | as_str | named_part | list_expr | ops | ops_self | ops_right)
            // (eof | any | ident | char_range | char | str | sub_expr | as_str | named_part | list_expr | ops | ops_self | ops_right)
            let alt_result = 
                (|| {
                    // eof
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_eof(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })()
                .alt(|| {
                    // any
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_any(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // ident
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_ident(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // char_range
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_char_range(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // char
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_char(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // str
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_str(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // sub_expr
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_sub_expr(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // as_str
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_as_str(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // named_part
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_named_part(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // list_expr
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_list_expr(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // ops
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_ops(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // ops_self
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_ops_self(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // ops_right
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_ops_right(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
            ;
            let (position, v0) = alt_result?;
            let value = Atom { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_eof(&mut self) -> Result<Eof<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_eof(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_eof(&self, position: Position) -> ParseResult<Eof<'i>> {
        // (value="EOF")
        let seq_result = node(position, |position| {
            // value="EOF"
            // "EOF"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "EOF").rule("eof").catch(&self.errors)?;
            let value = Eof { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_any(&mut self) -> Result<Any<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_any(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_any(&self, position: Position) -> ParseResult<Any<'i>> {
        // (value="ANY")
        let seq_result = node(position, |position| {
            // value="ANY"
            // "ANY"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "ANY").rule("any").catch(&self.errors)?;
            let value = Any { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ident(&mut self) -> Result<Ident<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_ident(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ident(&self, position: Position) -> ParseResult<Ident<'i>> {
        self.cache.ident_.cache(position, |position| {
            // (str=$((('a'..'z' | '_' | 'A'..'Z') ('a'..'z' | '_' | 'A'..'Z' | '0'..'9')*)))
            let seq_result = node(position, |position| {
                // str=$((('a'..'z' | '_' | 'A'..'Z') ('a'..'z' | '_' | 'A'..'Z' | '0'..'9')*))
                // $((('a'..'z' | '_' | 'A'..'Z') ('a'..'z' | '_' | 'A'..'Z' | '0'..'9')*))
                let as_str_result = self.input.as_str(position, |position| {
                    // (('a'..'z' | '_' | 'A'..'Z') ('a'..'z' | '_' | 'A'..'Z' | '0'..'9')*)
                    let seq_result = (|| {
                        // ('a'..'z' | '_' | 'A'..'Z')
                        let alt_result = 
                            (|| {
                                // 'a'..'z'
                                self.input.parse_char_range(position, 'a', 'z').rule("ident").catch(&self.errors).unit()
                            })()
                            .alt(|| {
                                // '_'
                                self.input.parse_char(position, '_').rule("ident").catch(&self.errors).unit()
                            })
                            .alt(|| {
                                // 'A'..'Z'
                                self.input.parse_char_range(position, 'A', 'Z').rule("ident").catch(&self.errors).unit()
                            })
                        ;
                        let (position, _) = alt_result?;
                        // ('a'..'z' | '_' | 'A'..'Z' | '0'..'9')*
                        let many_result = many(position, |position| {
                            // ('a'..'z' | '_' | 'A'..'Z' | '0'..'9')
                            let alt_result = 
                                (|| {
                                    // 'a'..'z'
                                    self.input.parse_char_range(position, 'a', 'z').rule("ident").catch(&self.errors).unit()
                                })()
                                .alt(|| {
                                    // '_'
                                    self.input.parse_char(position, '_').rule("ident").catch(&self.errors).unit()
                                })
                                .alt(|| {
                                    // 'A'..'Z'
                                    self.input.parse_char_range(position, 'A', 'Z').rule("ident").catch(&self.errors).unit()
                                })
                                .alt(|| {
                                    // '0'..'9'
                                    self.input.parse_char_range(position, '0', '9').rule("ident").catch(&self.errors).unit()
                                })
                            ;
                            alt_result
                        });
                        let (position, _) = many_result?;
                        Ok((position, ()))
                    })();
                    seq_result
                });
                let (position, v0) = as_str_result?;
                let value = Ident { str: v0.into(), span: Span::default(), };
                Ok((position, value))
            });
            let rule_result = seq_result;
            rule_result
        })
    }
    
    pub fn parse_char_range(&mut self) -> Result<CharRange<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_char_range(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_char_range(&self, position: Position) -> ParseResult<CharRange<'i>> {
        // (start=char ".." end=char)
        let seq_result = node(position, |position| {
            // start=char
            // char
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_char(position).rule("char_range").catch(&self.errors)?;
            // ".."
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_str(position, "..").rule("char_range").catch(&self.errors)?;
            // end=char
            // char
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_char(position).rule("char_range").catch(&self.errors)?;
            let value = CharRange { start: v0.into(), _0: v1.into(), end: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_char(&mut self) -> Result<Char<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_char(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_char(&self, position: Position) -> ParseResult<Char<'i>> {
        self.cache.char_.cache(position, |position| {
            // ('\'' char=(escape | char_escape | char_char) '\'')
            let seq_result = node(position, |position| {
                // '\''
                let (position, v0) = self.input.parse_char(position, '\'').rule("char").catch(&self.errors)?;
                // char=(escape | char_escape | char_char)
                // (escape | char_escape | char_char)
                let alt_result = 
                    (|| {
                        // escape
                        self._parse_escape(position).rule("char").catch(&self.errors).cast::<CharAlt>()
                    })()
                    .alt(|| {
                        // char_escape
                        self._parse_char_escape(position).rule("char").catch(&self.errors).cast::<CharAlt>()
                    })
                    .alt(|| {
                        // char_char
                        self._parse_char_char(position).rule("char").catch(&self.errors).cast::<CharAlt>()
                    })
                ;
                let (position, v1) = alt_result?;
                // '\''
                let (position, v2) = self.input.parse_char(position, '\'').rule("char").catch(&self.errors)?;
                let value = Char { _0: v0.into(), char: v1.into(), _1: v2.into(), span: Span::default(), };
                Ok((position, value))
            });
            let rule_result = seq_result;
            rule_result
        })
    }
    
    pub fn parse_str(&mut self) -> Result<Str<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_str(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_str(&self, position: Position) -> ParseResult<Str<'i>> {
        // ('\"' chars=(escape | str_escape | str_char)* '\"')
        let seq_result = node(position, |position| {
            // '\"'
            let (position, v0) = self.input.parse_char(position, '\"').rule("str").catch(&self.errors)?;
            // chars=(escape | str_escape | str_char)*
            // (escape | str_escape | str_char)*
            let many_result = many(position, |position| {
                // (escape | str_escape | str_char)
                let alt_result = 
                    (|| {
                        // escape
                        self._parse_escape(position).rule("str").catch(&self.errors).cast::<StrAlt>()
                    })()
                    .alt(|| {
                        // str_escape
                        self._parse_str_escape(position).rule("str").catch(&self.errors).cast::<StrAlt>()
                    })
                    .alt(|| {
                        // str_char
                        self._parse_str_char(position).rule("str").catch(&self.errors).cast::<StrAlt>()
                    })
                ;
                alt_result
            });
            let (position, v1) = many_result?;
            // '\"'
            let (position, v2) = self.input.parse_char(position, '\"').rule("str").catch(&self.errors)?;
            let value = Str { _0: v0.into(), chars: v1.into(), _1: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_sub_expr(&mut self) -> Result<SubExpr<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_sub_expr(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_sub_expr(&self, position: Position) -> ParseResult<SubExpr<'i>> {
        // ('(' value=alternative ')')
        let seq_result = node(position, |position| {
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '(').rule("sub_expr").catch(&self.errors)?;
            // value=alternative
            // alternative
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_alternative(position).rule("sub_expr").catch(&self.errors)?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, ')').rule("sub_expr").catch(&self.errors)?;
            let value = SubExpr { _0: v0.into(), value: v1.into(), _1: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_as_str(&mut self) -> Result<AsStr<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_as_str(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_as_str(&self, position: Position) -> ParseResult<AsStr<'i>> {
        // ("@str" '(' value=alternative ')')
        let seq_result = node(position, |position| {
            // "@str"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "@str").rule("as_str").catch(&self.errors)?;
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_char(position, '(').rule("as_str").catch(&self.errors)?;
            // value=alternative
            // alternative
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_alternative(position).rule("as_str").catch(&self.errors)?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self.input.parse_char(position, ')').rule("as_str").catch(&self.errors)?;
            let value = AsStr { _0: v0.into(), _1: v1.into(), value: v2.into(), _2: v3.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_named_part(&mut self) -> Result<NamedPart<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_named_part(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_named_part(&self, position: Position) -> ParseResult<NamedPart<'i>> {
        // ("@name" '(' name=ident ')')
        let seq_result = node(position, |position| {
            // "@name"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "@name").rule("named_part").catch(&self.errors)?;
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_char(position, '(').rule("named_part").catch(&self.errors)?;
            // name=ident
            // ident
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_ident(position).rule("named_part").catch(&self.errors)?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self.input.parse_char(position, ')').rule("named_part").catch(&self.errors)?;
            let value = NamedPart { _0: v0.into(), _1: v1.into(), name: v2.into(), _2: v3.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_list_expr(&mut self) -> Result<ListExpr<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_list_expr(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_list_expr(&self, position: Position) -> ParseResult<ListExpr<'i>> {
        // ("@list" '(' item=alternative ',' sep=alternative trailing=(',' "trailing")? ')')
        let seq_result = node(position, |position| {
            // "@list"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "@list").rule("list_expr").catch(&self.errors)?;
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_char(position, '(').rule("list_expr").catch(&self.errors)?;
            // item=alternative
            // alternative
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_alternative(position).rule("list_expr").catch(&self.errors)?;
            // ','
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self.input.parse_char(position, ',').rule("list_expr").catch(&self.errors)?;
            // sep=alternative
            // alternative
            let (position, _) = self._parse_skip(position)?;
            let (position, v4) = self._parse_alternative(position).rule("list_expr").catch(&self.errors)?;
            // trailing=(',' "trailing")?
            // (',' "trailing")?
            let opt_result = opt(position, |position| {
                // (',' "trailing")
                let seq_result = node(position, |position| {
                    // ','
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v0) = self.input.parse_char(position, ',').rule("list_expr").catch(&self.errors)?;
                    // "trailing"
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v1) = self.input.parse_str(position, "trailing").rule("list_expr").catch(&self.errors)?;
                    let value = ListExprSeq { _0: v0.into(), _1: v1.into(), span: Span::default(), };
                    Ok((position, value))
                });
                seq_result
            });
            let (position, v5) = opt_result?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v6) = self.input.parse_char(position, ')').rule("list_expr").catch(&self.errors)?;
            let value = ListExpr { _0: v0.into(), _1: v1.into(), item: v2.into(), _2: v3.into(), sep: v4.into(), trailing: v5.into(), _3: v6.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ops(&mut self) -> Result<Ops<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_ops(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ops(&self, position: Position) -> ParseResult<Ops<'i>> {
        // ("@ops" '(' value=alternative ')')
        let seq_result = node(position, |position| {
            // "@ops"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "@ops").rule("ops").catch(&self.errors)?;
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_char(position, '(').rule("ops").catch(&self.errors)?;
            // value=alternative
            // alternative
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_alternative(position).rule("ops").catch(&self.errors)?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self.input.parse_char(position, ')').rule("ops").catch(&self.errors)?;
            let value = Ops { _0: v0.into(), _1: v1.into(), value: v2.into(), _2: v3.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ops_self(&mut self) -> Result<OpsSelf<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_ops_self(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ops_self(&self, position: Position) -> ParseResult<OpsSelf<'i>> {
        // (value="@self")
        let seq_result = node(position, |position| {
            // value="@self"
            // "@self"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "@self").rule("ops_self").catch(&self.errors)?;
            let value = OpsSelf { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ops_right(&mut self) -> Result<OpsRight<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_ops_right(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ops_right(&self, position: Position) -> ParseResult<OpsRight<'i>> {
        // (value="@right")
        let seq_result = node(position, |position| {
            // value="@right"
            // "@right"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "@right").rule("ops_right").catch(&self.errors)?;
            let value = OpsRight { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_escape(&mut self) -> Result<Escape, Error> {
        let position = self.position;
        let (position, result) = self._parse_escape(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_escape(&self, position: Position) -> ParseResult<Escape> {
        // ('\\' value=('\\' | 'n' | 'r' | 't'))
        let seq_result = node(position, |position| {
            // '\\'
            let (position, v0) = self.input.parse_char(position, '\\').rule("escape").catch(&self.errors)?;
            // value=('\\' | 'n' | 'r' | 't')
            // ('\\' | 'n' | 'r' | 't')
            let alt_result = 
                (|| {
                    // '\\'
                    self.input.parse_char(position, '\\').rule("escape").catch(&self.errors)
                })()
                .alt(|| {
                    // 'n'
                    self.input.parse_char(position, 'n').rule("escape").catch(&self.errors)
                })
                .alt(|| {
                    // 'r'
                    self.input.parse_char(position, 'r').rule("escape").catch(&self.errors)
                })
                .alt(|| {
                    // 't'
                    self.input.parse_char(position, 't').rule("escape").catch(&self.errors)
                })
            ;
            let (position, v1) = alt_result?;
            let value = Escape { _0: v0.into(), value: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_char_char(&mut self) -> Result<CharChar, Error> {
        let position = self.position;
        let (position, result) = self._parse_char_char(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_char_char(&self, position: Position) -> ParseResult<CharChar> {
        // (value=ANY ~ ('\'' | '\n' | '\r'))
        let seq_result = node(position, |position| {
            // value=ANY ~ ('\'' | '\n' | '\r')
            // ANY ~ ('\'' | '\n' | '\r')
            let and_not_result = and_not(position, |position| {
                // ANY
                self.input.next(position).rule("char_char").catch(&self.errors)
                }, |position| {
                // ('\'' | '\n' | '\r')
                let alt_result = 
                    (|| {
                        // '\''
                        self.input.parse_char(position, '\'').rule("char_char").catch(&self.errors)
                    })()
                    .alt(|| {
                        // '\n'
                        self.input.parse_char(position, '\n').rule("char_char").catch(&self.errors)
                    })
                    .alt(|| {
                        // '\r'
                        self.input.parse_char(position, '\r').rule("char_char").catch(&self.errors)
                    })
                ;
                alt_result
            });
            let (position, v0) = and_not_result?;
            let value = CharChar { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_char_escape(&mut self) -> Result<CharEscape<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_char_escape(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_char_escape(&self, position: Position) -> ParseResult<CharEscape<'i>> {
        // (value="\\\'")
        let seq_result = node(position, |position| {
            // value="\\\'"
            // "\\\'"
            let (position, v0) = self.input.parse_str(position, "\\\'").rule("char_escape").catch(&self.errors)?;
            let value = CharEscape { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_str_char(&mut self) -> Result<StrChar, Error> {
        let position = self.position;
        let (position, result) = self._parse_str_char(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_str_char(&self, position: Position) -> ParseResult<StrChar> {
        // (value=ANY ~ ('\"' | '\n' | '\r'))
        let seq_result = node(position, |position| {
            // value=ANY ~ ('\"' | '\n' | '\r')
            // ANY ~ ('\"' | '\n' | '\r')
            let and_not_result = and_not(position, |position| {
                // ANY
                self.input.next(position).rule("str_char").catch(&self.errors)
                }, |position| {
                // ('\"' | '\n' | '\r')
                let alt_result = 
                    (|| {
                        // '\"'
                        self.input.parse_char(position, '\"').rule("str_char").catch(&self.errors)
                    })()
                    .alt(|| {
                        // '\n'
                        self.input.parse_char(position, '\n').rule("str_char").catch(&self.errors)
                    })
                    .alt(|| {
                        // '\r'
                        self.input.parse_char(position, '\r').rule("str_char").catch(&self.errors)
                    })
                ;
                alt_result
            });
            let (position, v0) = and_not_result?;
            let value = StrChar { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_str_escape(&mut self) -> Result<StrEscape<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_str_escape(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_str_escape(&self, position: Position) -> ParseResult<StrEscape<'i>> {
        // (value="\\\"")
        let seq_result = node(position, |position| {
            // value="\\\""
            // "\\\""
            let (position, v0) = self.input.parse_str(position, "\\\"").rule("str_escape").catch(&self.errors)?;
            let value = StrEscape { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_skip(&mut self) -> Result<Skip<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_skip(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_skip(&self, position: Position) -> ParseResult<Skip<'i>> {
        self.errors.stop();
        // (value=(ws | single_line_comment | multi_line_comment)*)
        let seq_result = node(position, |position| {
            // value=(ws | single_line_comment | multi_line_comment)*
            // (ws | single_line_comment | multi_line_comment)*
            let many_result = many(position, |position| {
                // (ws | single_line_comment | multi_line_comment)
                let alt_result = 
                    (|| {
                        // ws
                        self._parse_ws(position).rule("skip").catch(&self.errors).cast::<SkipAlt>()
                    })()
                    .alt(|| {
                        // single_line_comment
                        self._parse_single_line_comment(position).rule("skip").catch(&self.errors).cast::<SkipAlt>()
                    })
                    .alt(|| {
                        // multi_line_comment
                        self._parse_multi_line_comment(position).rule("skip").catch(&self.errors).cast::<SkipAlt>()
                    })
                ;
                alt_result
            });
            let (position, v0) = many_result?;
            let value = Skip { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        self.errors.start();
        rule_result
    }
    
    pub fn parse_single_line_comment(&mut self) -> Result<SingleLineComment<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_single_line_comment(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_single_line_comment(&self, position: Position) -> ParseResult<SingleLineComment<'i>> {
        // (str=$(("//" ANY ~ '\n'* '\n')))
        let seq_result = node(position, |position| {
            // str=$(("//" ANY ~ '\n'* '\n'))
            // $(("//" ANY ~ '\n'* '\n'))
            let as_str_result = self.input.as_str(position, |position| {
                // ("//" ANY ~ '\n'* '\n')
                let seq_result = (|| {
                    // "//"
                    let (position, _) = self.input.parse_str(position, "//").rule("single_line_comment").catch(&self.errors)?;
                    // ANY ~ '\n'*
                    let many_result = many(position, |position| {
                        // ANY ~ '\n'
                        let and_not_result = and_not(position, |position| {
                            // ANY
                            self.input.next(position).rule("single_line_comment").catch(&self.errors)
                            }, |position| {
                            // '\n'
                            self.input.parse_char(position, '\n').rule("single_line_comment").catch(&self.errors)
                        });
                        and_not_result
                    });
                    let (position, _) = many_result?;
                    // '\n'
                    let (position, _) = self.input.parse_char(position, '\n').rule("single_line_comment").catch(&self.errors)?;
                    Ok((position, ()))
                })();
                seq_result
            });
            let (position, v0) = as_str_result?;
            let value = SingleLineComment { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_multi_line_comment(&mut self) -> Result<MultiLineComment<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_multi_line_comment(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_multi_line_comment(&self, position: Position) -> ParseResult<MultiLineComment<'i>> {
        // (str=$(("/*" ANY ~ "*/"* "*/")))
        let seq_result = node(position, |position| {
            // str=$(("/*" ANY ~ "*/"* "*/"))
            // $(("/*" ANY ~ "*/"* "*/"))
            let as_str_result = self.input.as_str(position, |position| {
                // ("/*" ANY ~ "*/"* "*/")
                let seq_result = (|| {
                    // "/*"
                    let (position, _) = self.input.parse_str(position, "/*").rule("multi_line_comment").catch(&self.errors)?;
                    // ANY ~ "*/"*
                    let many_result = many(position, |position| {
                        // ANY ~ "*/"
                        let and_not_result = and_not(position, |position| {
                            // ANY
                            self.input.next(position).rule("multi_line_comment").catch(&self.errors)
                            }, |position| {
                            // "*/"
                            self.input.parse_str(position, "*/").rule("multi_line_comment").catch(&self.errors)
                        });
                        and_not_result
                    });
                    let (position, _) = many_result?;
                    // "*/"
                    let (position, _) = self.input.parse_str(position, "*/").rule("multi_line_comment").catch(&self.errors)?;
                    Ok((position, ()))
                })();
                seq_result
            });
            let (position, v0) = as_str_result?;
            let value = MultiLineComment { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ws(&mut self) -> Result<Ws<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_ws(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ws(&self, position: Position) -> ParseResult<Ws<'i>> {
        // (str=$((' ' | '\t' | '\r' | '\n')))
        let seq_result = node(position, |position| {
            // str=$((' ' | '\t' | '\r' | '\n'))
            // $((' ' | '\t' | '\r' | '\n'))
            let as_str_result = self.input.as_str(position, |position| {
                // (' ' | '\t' | '\r' | '\n')
                let alt_result = 
                    (|| {
                        // ' '
                        self.input.parse_char(position, ' ').rule("ws").catch(&self.errors).unit()
                    })()
                    .alt(|| {
                        // '\t'
                        self.input.parse_char(position, '\t').rule("ws").catch(&self.errors).unit()
                    })
                    .alt(|| {
                        // '\r'
                        self.input.parse_char(position, '\r').rule("ws").catch(&self.errors).unit()
                    })
                    .alt(|| {
                        // '\n'
                        self.input.parse_char(position, '\n').rule("ws").catch(&self.errors).unit()
                    })
                ;
                alt_result
            });
            let (position, v0) = as_str_result?;
            let value = Ws { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
}

// cache
#[derive(Clone, Debug, Default)]
struct Caches<'i> {
    ident_: Cache<Ident<'i>>,
    char_: Cache<Char<'i>>,
    _d: std::marker::PhantomData<&'i ()>,
}
// impls

impl<'i> Node for Decls<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Decls<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Decls")?;
        fmt.write_named("decls", &self.decls)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Rule<'i>> for DeclAlt<'i> {
    fn from(v: Rule<'i>) -> Self {
        Self::Rule(v)
    }
}
impl<'i> From<RulesOption<'i>> for DeclAlt<'i> {
    fn from(v: RulesOption<'i>) -> Self {
        Self::RulesOption(v)
    }
}
impl<'i> Node for DeclAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Rule(value) => value.set_span(span),
            Self::RulesOption(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Rule(value) => value.get_span(),
            Self::RulesOption(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for DeclAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Rule(value) => value.format(fmt),
            Self::RulesOption(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Decl<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Decl<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Decl")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for RulesOption<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for RulesOption<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("RulesOption")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for RuleSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for RuleSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("RuleSeq")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("modifiers", &self.modifiers)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Rule<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Rule<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Rule")?;
        fmt.write_named("name", &self.name)?;
        fmt.write_named("modifiers", &self.modifiers)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("body", &self.body)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Modifier<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Modifier<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Modifier")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for AlternativeSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for AlternativeSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("AlternativeSeq")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("seq", &self.seq)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Alternative<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Alternative<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Alternative")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("first", &self.first)?;
        fmt.write_named("rest", &self.rest)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Sequence<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Sequence<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Sequence")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for NamedSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for NamedSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("NamedSeq")?;
        fmt.write_named("name", &self.name)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Named<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Named<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Named")?;
        fmt.write_named("name", &self.name)?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<char> for PostfixAlt<'i> {
    fn from(v: char) -> Self {
        Self::Char(v)
    }
}
impl<'i> From<AndNot<'i>> for PostfixAlt<'i> {
    fn from(v: AndNot<'i>) -> Self {
        Self::AndNot(v)
    }
}
impl<'i> Node for PostfixAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Char(_) => {}
            Self::AndNot(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Char(_) => Span::default(),
            Self::AndNot(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for PostfixAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Char(value) => value.format(fmt),
            Self::AndNot(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Postfix<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Postfix<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Postfix")?;
        fmt.write_named("atom", &self.atom)?;
        fmt.write_named("ops", &self.ops)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for AndNot<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for AndNot<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("AndNot")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("not", &self.not)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Eof<'i>> for AtomAlt<'i> {
    fn from(v: Eof<'i>) -> Self {
        Self::Eof(v)
    }
}
impl<'i> From<Any<'i>> for AtomAlt<'i> {
    fn from(v: Any<'i>) -> Self {
        Self::Any(v)
    }
}
impl<'i> From<Ident<'i>> for AtomAlt<'i> {
    fn from(v: Ident<'i>) -> Self {
        Self::Ident(v)
    }
}
impl<'i> From<CharRange<'i>> for AtomAlt<'i> {
    fn from(v: CharRange<'i>) -> Self {
        Self::CharRange(v)
    }
}
impl<'i> From<Char<'i>> for AtomAlt<'i> {
    fn from(v: Char<'i>) -> Self {
        Self::Char(v)
    }
}
impl<'i> From<Str<'i>> for AtomAlt<'i> {
    fn from(v: Str<'i>) -> Self {
        Self::Str(v)
    }
}
impl<'i> From<SubExpr<'i>> for AtomAlt<'i> {
    fn from(v: SubExpr<'i>) -> Self {
        Self::SubExpr(v)
    }
}
impl<'i> From<AsStr<'i>> for AtomAlt<'i> {
    fn from(v: AsStr<'i>) -> Self {
        Self::AsStr(v)
    }
}
impl<'i> From<NamedPart<'i>> for AtomAlt<'i> {
    fn from(v: NamedPart<'i>) -> Self {
        Self::NamedPart(v)
    }
}
impl<'i> From<ListExpr<'i>> for AtomAlt<'i> {
    fn from(v: ListExpr<'i>) -> Self {
        Self::ListExpr(v)
    }
}
impl<'i> From<Ops<'i>> for AtomAlt<'i> {
    fn from(v: Ops<'i>) -> Self {
        Self::Ops(v)
    }
}
impl<'i> From<OpsSelf<'i>> for AtomAlt<'i> {
    fn from(v: OpsSelf<'i>) -> Self {
        Self::OpsSelf(v)
    }
}
impl<'i> From<OpsRight<'i>> for AtomAlt<'i> {
    fn from(v: OpsRight<'i>) -> Self {
        Self::OpsRight(v)
    }
}
impl<'i> Node for AtomAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Eof(value) => value.set_span(span),
            Self::Any(value) => value.set_span(span),
            Self::Ident(value) => value.set_span(span),
            Self::CharRange(value) => value.set_span(span),
            Self::Char(value) => value.set_span(span),
            Self::Str(value) => value.set_span(span),
            Self::SubExpr(value) => value.set_span(span),
            Self::AsStr(value) => value.set_span(span),
            Self::NamedPart(value) => value.set_span(span),
            Self::ListExpr(value) => value.set_span(span),
            Self::Ops(value) => value.set_span(span),
            Self::OpsSelf(value) => value.set_span(span),
            Self::OpsRight(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Eof(value) => value.get_span(),
            Self::Any(value) => value.get_span(),
            Self::Ident(value) => value.get_span(),
            Self::CharRange(value) => value.get_span(),
            Self::Char(value) => value.get_span(),
            Self::Str(value) => value.get_span(),
            Self::SubExpr(value) => value.get_span(),
            Self::AsStr(value) => value.get_span(),
            Self::NamedPart(value) => value.get_span(),
            Self::ListExpr(value) => value.get_span(),
            Self::Ops(value) => value.get_span(),
            Self::OpsSelf(value) => value.get_span(),
            Self::OpsRight(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for AtomAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Eof(value) => value.format(fmt),
            Self::Any(value) => value.format(fmt),
            Self::Ident(value) => value.format(fmt),
            Self::CharRange(value) => value.format(fmt),
            Self::Char(value) => value.format(fmt),
            Self::Str(value) => value.format(fmt),
            Self::SubExpr(value) => value.format(fmt),
            Self::AsStr(value) => value.format(fmt),
            Self::NamedPart(value) => value.format(fmt),
            Self::ListExpr(value) => value.format(fmt),
            Self::Ops(value) => value.format(fmt),
            Self::OpsSelf(value) => value.format(fmt),
            Self::OpsRight(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Atom<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Atom<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Atom")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Eof<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Eof<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Eof")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Any<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Any<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Any")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Ident<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Ident<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Ident")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for CharRange<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for CharRange<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("CharRange")?;
        fmt.write_named("start", &self.start)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("end", &self.end)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Escape> for CharAlt<'i> {
    fn from(v: Escape) -> Self {
        Self::Escape(v)
    }
}
impl<'i> From<CharEscape<'i>> for CharAlt<'i> {
    fn from(v: CharEscape<'i>) -> Self {
        Self::CharEscape(v)
    }
}
impl<'i> From<CharChar> for CharAlt<'i> {
    fn from(v: CharChar) -> Self {
        Self::CharChar(v)
    }
}
impl<'i> Node for CharAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Escape(value) => value.set_span(span),
            Self::CharEscape(value) => value.set_span(span),
            Self::CharChar(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Escape(value) => value.get_span(),
            Self::CharEscape(value) => value.get_span(),
            Self::CharChar(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for CharAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Escape(value) => value.format(fmt),
            Self::CharEscape(value) => value.format(fmt),
            Self::CharChar(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Char<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Char<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Char")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("char", &self.char)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Escape> for StrAlt<'i> {
    fn from(v: Escape) -> Self {
        Self::Escape(v)
    }
}
impl<'i> From<StrEscape<'i>> for StrAlt<'i> {
    fn from(v: StrEscape<'i>) -> Self {
        Self::StrEscape(v)
    }
}
impl<'i> From<StrChar> for StrAlt<'i> {
    fn from(v: StrChar) -> Self {
        Self::StrChar(v)
    }
}
impl<'i> Node for StrAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Escape(value) => value.set_span(span),
            Self::StrEscape(value) => value.set_span(span),
            Self::StrChar(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Escape(value) => value.get_span(),
            Self::StrEscape(value) => value.get_span(),
            Self::StrChar(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for StrAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Escape(value) => value.format(fmt),
            Self::StrEscape(value) => value.format(fmt),
            Self::StrChar(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Str<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Str<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Str")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("chars", &self.chars)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for SubExpr<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for SubExpr<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("SubExpr")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("value", &self.value)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for AsStr<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for AsStr<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("AsStr")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("value", &self.value)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for NamedPart<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for NamedPart<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("NamedPart")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("name", &self.name)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ListExprSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ListExprSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ListExprSeq")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ListExpr<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ListExpr<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ListExpr")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("item", &self.item)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_named("sep", &self.sep)?;
        fmt.write_named("trailing", &self.trailing)?;
        fmt.write_unnamed("_3", &self._3)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Ops<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Ops<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Ops")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("value", &self.value)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for OpsSelf<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for OpsSelf<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("OpsSelf")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for OpsRight<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for OpsRight<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("OpsRight")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl Node for Escape {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl TreeDisplay for Escape {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Escape")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl Node for CharChar {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl TreeDisplay for CharChar {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("CharChar")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for CharEscape<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for CharEscape<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("CharEscape")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl Node for StrChar {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl TreeDisplay for StrChar {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("StrChar")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for StrEscape<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for StrEscape<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("StrEscape")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Ws<'i>> for SkipAlt<'i> {
    fn from(v: Ws<'i>) -> Self {
        Self::Ws(v)
    }
}
impl<'i> From<SingleLineComment<'i>> for SkipAlt<'i> {
    fn from(v: SingleLineComment<'i>) -> Self {
        Self::SingleLineComment(v)
    }
}
impl<'i> From<MultiLineComment<'i>> for SkipAlt<'i> {
    fn from(v: MultiLineComment<'i>) -> Self {
        Self::MultiLineComment(v)
    }
}
impl<'i> Node for SkipAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Ws(value) => value.set_span(span),
            Self::SingleLineComment(value) => value.set_span(span),
            Self::MultiLineComment(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Ws(value) => value.get_span(),
            Self::SingleLineComment(value) => value.get_span(),
            Self::MultiLineComment(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for SkipAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Ws(value) => value.format(fmt),
            Self::SingleLineComment(value) => value.format(fmt),
            Self::MultiLineComment(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Skip<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Skip<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Skip")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for SingleLineComment<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for SingleLineComment<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("SingleLineComment")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for MultiLineComment<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for MultiLineComment<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("MultiLineComment")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Ws<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Ws<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Ws")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}