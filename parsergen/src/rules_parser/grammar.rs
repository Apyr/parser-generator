use crate::Rules;

const GRAMMAR: &str = r#"
decls: decls=decl* EOF;
decl: rule | rules_option;
rules_option: "@@save_skips" ';';
rule:
    name=ident
    modifiers=('(' modifiers=@list(modifier, ',') ')')?
    //modifiers=('(' modifiers=(modifier ,+ ',') ')')?
    ':'
    body=alternative
    ';'
;
modifier(str): "str" | "atomic" | "cached" | "operators";

alternative: '|'? first=sequence rest=('|' seq=sequence)*;
sequence: named+;
named: name=(name=ident '=')? value=postfix;
postfix: atom=atom ops=('*' | '+' | '?' | and_not)*;
and_not: '~' not=alternative;
atom:
    eof | any | ident 
    | char_range | char | str
    | sub_expr 
    | as_str | named_part | list_expr
    | ops | ops_self | ops_right
;

eof: "EOF";
any: "ANY";
ident(str, cached):
    ('a'..'z' | '_' | 'A'..'Z')
    ('a'..'z' | '_' | 'A'..'Z' | '0'..'9')*
;
char_range: start=char ".." end=char;
char(atomic, cached): '\'' char=(escape | char_escape | char_char) '\'';
str(atomic): '"' chars=(escape | str_escape | str_char)* '"';
sub_expr: '(' value=alternative ')';
as_str: "@str" '(' value=alternative ')';
named_part: "@name" '(' name=ident ')';
list_expr:
    "@list" '('
    item=alternative
    ',' sep=alternative
    trailing=(',' "trailing")?
    ')'
;
ops: "@ops" '(' value=alternative ')';
ops_self: "@self";
ops_right: "@right";

escape(atomic): '\\' value=('\\' | 'n' | 'r' | 't');
char_char(atomic): ANY ~ ('\'' | '\n' | '\r');
char_escape(atomic): "\\'";
str_char(atomic): ANY ~ ('"' | '\n' | '\r');
str_escape(atomic): "\\\"";

skip: (ws | single_line_comment | multi_line_comment)*;
single_line_comment(str): "//" (ANY~'\n')* '\n';
multi_line_comment(str): "/*" (ANY~"*/")* "*/";
ws(str): ' ' | '\t' | '\r' | '\n';
"#;

pub fn parser_grammar() -> Rules {
    super::compile_rules(GRAMMAR).unwrap()
}
