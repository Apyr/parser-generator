use parsergen::rules_parser::compile_rules;
use std::fs::write;

fn main() {
    let grammar = r#"
        expr(operators):
            @name(bin_op) left=@right op=("=" | "+=" | "-=" | "*=" | "/=" | "%=") right=@self
            | @name(bin_op) left=@self op=("||" | "or") right=@self
            | @name(bin_op) left=@self op=("&&" | "and") right=@self
            | @name(bin_op) left=@self op=("==" | "!=") right=@self
            | @name(bin_op) left=@self op=(">=" | "<=" | "<" | ">") right=@self
            | @name(bin_op) left=@self op=("+" | "-") right=@self
            | @name(bin_op) left=@self op=("*" | "/" | "%") right=@self
            | atom
            ;

        atom: ident | digit;
        ident(str): ('a'..'z' | '_' | 'A'..'Z') ('a'..'z' | '_' | 'A'..'Z')*;
        digit(str): '0'..'9'+; 

        skip(str, cached): (' ' | '\t' | '\r' | '\n')*;
    "#;

    let rules = compile_rules(grammar).unwrap();

    let usage = r#"

fn main() {
    let text = "a = b /= 1 + 2 * 3 / 4";
    let value = Parser::new(text).parse_expr().unwrap();
    println!("{}", Config::default().display(value));
}
"#;
    write("examples/ops_parser.rs", rules.generate() + &usage).unwrap();
}
