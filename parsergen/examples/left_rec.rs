use parsergen::rules_parser::compile_rules;
use std::fs::write;

fn main() {
    let grammar = r#"
        expr(left_rec): binop@ left=expr op=('+' | '-') right=expr1 | expr1;
        expr1(left_rec): binop1@ left=expr1 op=('*' | '/') right=term | term;
        term(atomic): number | subexpr@ '(' expr=expr ')';
        number(str): ('0'..'9')+;
        skip(str): (' ' | '\t' | '\r' | '\n')*;
    "#;

    let usage = r#"

fn main() {
    let text = "1 + 2 + 3 * 4 / (5 + 6)";
    let value = Parser::new(text).parse_expr().unwrap();
    println!("{}", Config::default().display(value));
}
"#;

    let rules = compile_rules(grammar).unwrap();
    write("examples/left_rec_parser.rs", rules.generate() + &usage).unwrap();
}
