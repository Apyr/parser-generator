use parsergen::rules_parser::compile_rules;
use std::fs::write;

fn main() {
    let grammar = r#"
        value(ops, cached): object | array | null | false | true | number | string;
        
        object: '{' content=@list(pair, ',') '}';
        pair(cached): key=string ':' value=value;
        array: '[' content=@list(value, ',') ']';

        null: "null";
        false: "false";
        true: "true";

        number(str): '-'? digit+ ('.' digit*)?;
        digit(atomic): '0'..'9';

        string(cached, atomic): '"' chars=char* '"';
        char(atomic): "\\\\" | "\\n" | "\\r" | "\\t" | (ANY ~ '"');

        skip(str, cached): (' ' | '\t' | '\r' | '\n')*;
    "#;

    let usage = r#"

fn main() {
    let json = "{\n\"hello\": [\"world\", 3.14 , false, null, true, -5, 123],\n \"world\": \"world\"\n}";
    let value = Parser::new(json).parse_value().unwrap();
    println!("{}", Config::default().display(value));
}
"#;

    let rules = compile_rules(grammar).unwrap();
    //println!("{}", rules);
    write("examples/json_parser.rs", rules.generate() + &usage).unwrap();
}
