use parsergen::rules_parser::parser_grammar;
use std::fs::write;

fn main() {
    let rules = parser_grammar();
    let generated = rules.generate();
    write("src/rules_parser/parser.rs", generated).unwrap();
}
