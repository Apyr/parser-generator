use parsergen_runtime::*;

// types

#[derive(Clone, Debug)]
pub enum DeclsAlt<'i> {
    End(End<'i>),
    Decl(Decl<'i>),
}

#[derive(Clone, Debug)]
pub struct Decls<'i> {
    pub decls: Vec<DeclsAlt<'i>>,
    pub _0: (),
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum DeclAlt<'i> {
    Struct(Struct<'i>),
    Func(Func<'i>),
}

#[derive(Clone, Debug)]
pub struct Decl<'i> {
    pub value: DeclAlt<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Struct<'i> {
    pub _0: &'i str,
    pub name: Ident<'i>,
    pub _1: char,
    pub fields: Args<'i>,
    pub _2: char,
    pub _3: End<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct FuncSeq<'i> {
    pub _0: char,
    pub ty: Type<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Func<'i> {
    pub _0: &'i str,
    pub name: Ident<'i>,
    pub _1: char,
    pub args: Option<Args<'i>>,
    pub _2: char,
    pub return_type: Option<FuncSeq<'i>>,
    pub body: Block<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ArgsSeq<'i> {
    pub _0: char,
    pub arg: Arg<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Args<'i> {
    pub first: Arg<'i>,
    pub rest: ArgsSeq<'i>,
    pub _0: Option<char>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Arg<'i> {
    pub name: Ident<'i>,
    pub _0: char,
    pub ty: Type<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum TypeAlt<'i> {
    PtrType(PtrType<'i>),
    ArrayType(ArrayType<'i>),
    Ident(Ident<'i>),
}

#[derive(Clone, Debug)]
pub struct Type<'i> {
    pub ty: Box<TypeAlt<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct PtrType<'i> {
    pub _0: char,
    pub ty: Box<Type<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ArrayType<'i> {
    pub _0: &'i str,
    pub _1: char,
    pub ty: Type<'i>,
    pub _2: char,
    pub size: Uint<'i>,
    pub _3: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum BlockAlt<'i> {
    End(End<'i>),
    Stmt(Stmt<'i>),
}

#[derive(Clone, Debug)]
pub struct Block<'i> {
    pub _0: char,
    pub stmts: Vec<BlockAlt<'i>>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum StmtAlt<'i> {
    Return(Return<'i>),
    While(While<'i>),
    Var(Var<'i>),
    ExprStmt(ExprStmt<'i>),
}

#[derive(Clone, Debug)]
pub struct Stmt<'i> {
    pub value: StmtAlt<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Return<'i> {
    pub _0: &'i str,
    pub result: Option<Expr<'i>>,
    pub _1: End<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct While<'i> {
    pub _0: &'i str,
    pub cond: Expr<'i>,
    pub body: Block<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ExprStmt<'i> {
    pub expr: Expr<'i>,
    pub _0: End<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct VarSeq<'i> {
    pub _0: char,
    pub ty: Type<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Var<'i> {
    pub _0: &'i str,
    pub name: Ident<'i>,
    pub ty: Option<VarSeq<'i>>,
    pub _1: char,
    pub init: Expr<'i>,
    pub _2: End<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ExprAltSeq<'i> {
    pub left: Box<ExprAlt<'i>>,
    pub op: &'i str,
    pub right: Box<ExprAlt<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum ExprAlt<'i> {
    ExprAltSeq(ExprAltSeq<'i>),
    Prefix(Prefix<'i>),
}

#[derive(Clone, Debug)]
pub struct Expr<'i> {
    pub value: Box<ExprAlt<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum PrefixAlt<'i> {
    Char(char),
    NotOp(NotOp<'i>),
}

#[derive(Clone, Debug)]
pub struct Prefix<'i> {
    pub ops: Vec<PrefixAlt<'i>>,
    pub _0: Postfix<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct NotOp<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum PostfixAlt<'i> {
    DotAccess(DotAccess<'i>),
    ArrayAccess(ArrayAccess<'i>),
    Call(Call<'i>),
}

#[derive(Clone, Debug)]
pub struct Postfix<'i> {
    pub expr: Atom<'i>,
    pub ops: Vec<PostfixAlt<'i>>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct DotAccess<'i> {
    pub _0: char,
    pub field: Ident<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ArrayAccess<'i> {
    pub _0: char,
    pub index: Expr<'i>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Call<'i> {
    pub _0: char,
    pub args: Option<ExprArgs<'i>>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ExprArgsSeq<'i> {
    pub _0: char,
    pub arg: Expr<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct ExprArgs<'i> {
    pub first: Expr<'i>,
    pub rest: ExprArgsSeq<'i>,
    pub _0: Option<char>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum AtomAlt<'i> {
    Float(Float<'i>),
    Uint(Uint<'i>),
    False(False<'i>),
    True(True<'i>),
    Null(Null<'i>),
    Ident(Ident<'i>),
    SubExpr(SubExpr<'i>),
}

#[derive(Clone, Debug)]
pub struct Atom<'i> {
    pub value: AtomAlt<'i>,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct False<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct True<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Null<'i> {
    pub value: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct SubExpr<'i> {
    pub _0: char,
    pub expr: Box<Expr<'i>>,
    pub _1: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct End<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Ident<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Uint<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Float<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Letter {
    pub value: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Digit {
    pub value: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Ws {
    pub value: char,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub struct Comment<'i> {
    pub str: &'i str,
    pub span: Span,
}

#[derive(Clone, Debug)]
pub enum SkipAlt<'i> {
    Ws(Ws),
    Comment(Comment<'i>),
}

#[derive(Clone, Debug)]
pub struct Skip<'i> {
    pub value: Vec<SkipAlt<'i>>,
    pub span: Span,
}

// parser

#[allow(dead_code)]
#[derive(Clone, Debug)]
pub struct Parser<'i> {
    position: Position,
    input: Input<'i>,
    errors: Errors,
    cache: Caches<'i>,
}

impl<'i> Parser<'i> {
    pub fn new(text: &'i str) -> Self {
        Parser {
            position: Position::default(),
            input: Input(text),
            errors: Errors::new(),
            cache: Caches::default(),
        }
    }
}

#[allow(dead_code)]
impl<'i> Parser<'i> {
    pub fn parse_decls(&mut self) -> Result<Decls<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_decls(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_decls(&self, position: Position) -> ParseResult<Decls<'i>> {
        // (decls=(end | decl)* EOF)
        let seq_result = node(position, |position| {
            // decls=(end | decl)*
            // (end | decl)*
            let many_result = many(position, |position| {
                // (end | decl)
                let alt_result = 
                    (|| {
                        // end
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_end(position).rule("decls").catch(&self.errors).cast::<DeclsAlt>()
                    })()
                    .alt(|| {
                        // decl
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_decl(position).rule("decls").catch(&self.errors).cast::<DeclsAlt>()
                    })
                ;
                alt_result
            });
            let (position, v0) = many_result?;
            // EOF
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_eof(position).rule("decls").catch(&self.errors)?;
            let value = Decls { decls: v0.into(), _0: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_decl(&mut self) -> Result<Decl<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_decl(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_decl(&self, position: Position) -> ParseResult<Decl<'i>> {
        // (value=(struct | func))
        let seq_result = node(position, |position| {
            // value=(struct | func)
            // (struct | func)
            let alt_result = 
                (|| {
                    // struct
                    self._parse_struct(position).rule("decl").catch(&self.errors).cast::<DeclAlt>()
                })()
                .alt(|| {
                    // func
                    self._parse_func(position).rule("decl").catch(&self.errors).cast::<DeclAlt>()
                })
            ;
            let (position, v0) = alt_result?;
            let value = Decl { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_struct(&mut self) -> Result<Struct<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_struct(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_struct(&self, position: Position) -> ParseResult<Struct<'i>> {
        // ("struct" name=ident '(' fields=args ')' end)
        let seq_result = node(position, |position| {
            // "struct"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "struct").rule("struct").catch(&self.errors)?;
            // name=ident
            // ident
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_ident(position).rule("struct").catch(&self.errors)?;
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, '(').rule("struct").catch(&self.errors)?;
            // fields=args
            // args
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self._parse_args(position).rule("struct").catch(&self.errors)?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v4) = self.input.parse_char(position, ')').rule("struct").catch(&self.errors)?;
            // end
            let (position, _) = self._parse_skip(position)?;
            let (position, v5) = self._parse_end(position).rule("struct").catch(&self.errors)?;
            let value = Struct { _0: v0.into(), name: v1.into(), _1: v2.into(), fields: v3.into(), _2: v4.into(), _3: v5.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_func(&mut self) -> Result<Func<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_func(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_func(&self, position: Position) -> ParseResult<Func<'i>> {
        // ("fn" name=ident '(' args=args? ')' return_type=(':' ty=type)? body=block)
        let seq_result = node(position, |position| {
            // "fn"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "fn").rule("func").catch(&self.errors)?;
            // name=ident
            // ident
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_ident(position).rule("func").catch(&self.errors)?;
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, '(').rule("func").catch(&self.errors)?;
            // args=args?
            // args?
            let opt_result = opt(position, |position| {
                // args
                let (position, _) = self._parse_skip(position)?;
                self._parse_args(position).rule("func").catch(&self.errors)
            });
            let (position, v3) = opt_result?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v4) = self.input.parse_char(position, ')').rule("func").catch(&self.errors)?;
            // return_type=(':' ty=type)?
            // (':' ty=type)?
            let opt_result = opt(position, |position| {
                // (':' ty=type)
                let seq_result = node(position, |position| {
                    // ':'
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v0) = self.input.parse_char(position, ':').rule("func").catch(&self.errors)?;
                    // ty=type
                    // type
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v1) = self._parse_type(position).rule("func").catch(&self.errors)?;
                    let value = FuncSeq { _0: v0.into(), ty: v1.into(), span: Span::default(), };
                    Ok((position, value))
                });
                seq_result
            });
            let (position, v5) = opt_result?;
            // body=block
            // block
            let (position, _) = self._parse_skip(position)?;
            let (position, v6) = self._parse_block(position).rule("func").catch(&self.errors)?;
            let value = Func { _0: v0.into(), name: v1.into(), _1: v2.into(), args: v3.into(), _2: v4.into(), return_type: v5.into(), body: v6.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_args(&mut self) -> Result<Args<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_args(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_args(&self, position: Position) -> ParseResult<Args<'i>> {
        // (first=arg rest=(',' arg=arg) ','?)
        let seq_result = node(position, |position| {
            // first=arg
            // arg
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_arg(position).rule("args").catch(&self.errors)?;
            // rest=(',' arg=arg)
            // (',' arg=arg)
            let seq_result = node(position, |position| {
                // ','
                let (position, _) = self._parse_skip(position)?;
                let (position, v0) = self.input.parse_char(position, ',').rule("args").catch(&self.errors)?;
                // arg=arg
                // arg
                let (position, _) = self._parse_skip(position)?;
                let (position, v1) = self._parse_arg(position).rule("args").catch(&self.errors)?;
                let value = ArgsSeq { _0: v0.into(), arg: v1.into(), span: Span::default(), };
                Ok((position, value))
            });
            let (position, v1) = seq_result?;
            // ','?
            let opt_result = opt(position, |position| {
                // ','
                let (position, _) = self._parse_skip(position)?;
                self.input.parse_char(position, ',').rule("args").catch(&self.errors)
            });
            let (position, v2) = opt_result?;
            let value = Args { first: v0.into(), rest: v1.into(), _0: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_arg(&mut self) -> Result<Arg<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_arg(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_arg(&self, position: Position) -> ParseResult<Arg<'i>> {
        // (name=ident ':' ty=type)
        let seq_result = node(position, |position| {
            // name=ident
            // ident
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_ident(position).rule("arg").catch(&self.errors)?;
            // ':'
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_char(position, ':').rule("arg").catch(&self.errors)?;
            // ty=type
            // type
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_type(position).rule("arg").catch(&self.errors)?;
            let value = Arg { name: v0.into(), _0: v1.into(), ty: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_type(&mut self) -> Result<Type<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_type(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_type(&self, position: Position) -> ParseResult<Type<'i>> {
        // (ty=(ptr_type | array_type | ident))
        let seq_result = node(position, |position| {
            // ty=(ptr_type | array_type | ident)
            // (ptr_type | array_type | ident)
            let alt_result = 
                (|| {
                    // ptr_type
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_ptr_type(position).rule("type").catch(&self.errors).cast::<TypeAlt>()
                })()
                .alt(|| {
                    // array_type
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_array_type(position).rule("type").catch(&self.errors).cast::<TypeAlt>()
                })
                .alt(|| {
                    // ident
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_ident(position).rule("type").catch(&self.errors).cast::<TypeAlt>()
                })
            ;
            let (position, v0) = alt_result?;
            let value = Type { ty: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ptr_type(&mut self) -> Result<PtrType<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_ptr_type(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ptr_type(&self, position: Position) -> ParseResult<PtrType<'i>> {
        // ('*' ty=type)
        let seq_result = node(position, |position| {
            // '*'
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '*').rule("ptr_type").catch(&self.errors)?;
            // ty=type
            // type
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_type(position).rule("ptr_type").catch(&self.errors)?;
            let value = PtrType { _0: v0.into(), ty: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_array_type(&mut self) -> Result<ArrayType<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_array_type(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_array_type(&self, position: Position) -> ParseResult<ArrayType<'i>> {
        // ("Array" '[' ty=type ',' size=uint ']')
        let seq_result = node(position, |position| {
            // "Array"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "Array").rule("array_type").catch(&self.errors)?;
            // '['
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self.input.parse_char(position, '[').rule("array_type").catch(&self.errors)?;
            // ty=type
            // type
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_type(position).rule("array_type").catch(&self.errors)?;
            // ','
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self.input.parse_char(position, ',').rule("array_type").catch(&self.errors)?;
            // size=uint
            // uint
            let (position, _) = self._parse_skip(position)?;
            let (position, v4) = self._parse_uint(position).rule("array_type").catch(&self.errors)?;
            // ']'
            let (position, _) = self._parse_skip(position)?;
            let (position, v5) = self.input.parse_char(position, ']').rule("array_type").catch(&self.errors)?;
            let value = ArrayType { _0: v0.into(), _1: v1.into(), ty: v2.into(), _2: v3.into(), size: v4.into(), _3: v5.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_block(&mut self) -> Result<Block<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_block(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_block(&self, position: Position) -> ParseResult<Block<'i>> {
        // ('{' stmts=(end | stmt)* '}')
        let seq_result = node(position, |position| {
            // '{'
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '{').rule("block").catch(&self.errors)?;
            // stmts=(end | stmt)*
            // (end | stmt)*
            let many_result = many(position, |position| {
                // (end | stmt)
                let alt_result = 
                    (|| {
                        // end
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_end(position).rule("block").catch(&self.errors).cast::<BlockAlt>()
                    })()
                    .alt(|| {
                        // stmt
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_stmt(position).rule("block").catch(&self.errors).cast::<BlockAlt>()
                    })
                ;
                alt_result
            });
            let (position, v1) = many_result?;
            // '}'
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, '}').rule("block").catch(&self.errors)?;
            let value = Block { _0: v0.into(), stmts: v1.into(), _1: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_stmt(&mut self) -> Result<Stmt<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_stmt(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_stmt(&self, position: Position) -> ParseResult<Stmt<'i>> {
        // (value=(return | while | var | expr_stmt))
        let seq_result = node(position, |position| {
            // value=(return | while | var | expr_stmt)
            // (return | while | var | expr_stmt)
            let alt_result = 
                (|| {
                    // return
                    self._parse_return(position).rule("stmt").catch(&self.errors).cast::<StmtAlt>()
                })()
                .alt(|| {
                    // while
                    self._parse_while(position).rule("stmt").catch(&self.errors).cast::<StmtAlt>()
                })
                .alt(|| {
                    // var
                    self._parse_var(position).rule("stmt").catch(&self.errors).cast::<StmtAlt>()
                })
                .alt(|| {
                    // expr_stmt
                    self._parse_expr_stmt(position).rule("stmt").catch(&self.errors).cast::<StmtAlt>()
                })
            ;
            let (position, v0) = alt_result?;
            let value = Stmt { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_return(&mut self) -> Result<Return<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_return(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_return(&self, position: Position) -> ParseResult<Return<'i>> {
        // ("return" result=expr? end)
        let seq_result = node(position, |position| {
            // "return"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "return").rule("return").catch(&self.errors)?;
            // result=expr?
            // expr?
            let opt_result = opt(position, |position| {
                // expr
                let (position, _) = self._parse_skip(position)?;
                self._parse_expr(position).rule("return").catch(&self.errors)
            });
            let (position, v1) = opt_result?;
            // end
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_end(position).rule("return").catch(&self.errors)?;
            let value = Return { _0: v0.into(), result: v1.into(), _1: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_while(&mut self) -> Result<While<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_while(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_while(&self, position: Position) -> ParseResult<While<'i>> {
        // ("while" cond=expr body=block)
        let seq_result = node(position, |position| {
            // "while"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "while").rule("while").catch(&self.errors)?;
            // cond=expr
            // expr
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_expr(position).rule("while").catch(&self.errors)?;
            // body=block
            // block
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self._parse_block(position).rule("while").catch(&self.errors)?;
            let value = While { _0: v0.into(), cond: v1.into(), body: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_expr_stmt(&mut self) -> Result<ExprStmt<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_expr_stmt(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_expr_stmt(&self, position: Position) -> ParseResult<ExprStmt<'i>> {
        // (expr=expr end)
        let seq_result = node(position, |position| {
            // expr=expr
            // expr
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_expr(position).rule("expr_stmt").catch(&self.errors)?;
            // end
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_end(position).rule("expr_stmt").catch(&self.errors)?;
            let value = ExprStmt { expr: v0.into(), _0: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_var(&mut self) -> Result<Var<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_var(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_var(&self, position: Position) -> ParseResult<Var<'i>> {
        // ("var" name=ident ty=(':' ty=type)? '=' init=expr end)
        let seq_result = node(position, |position| {
            // "var"
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_str(position, "var").rule("var").catch(&self.errors)?;
            // name=ident
            // ident
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_ident(position).rule("var").catch(&self.errors)?;
            // ty=(':' ty=type)?
            // (':' ty=type)?
            let opt_result = opt(position, |position| {
                // (':' ty=type)
                let seq_result = node(position, |position| {
                    // ':'
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v0) = self.input.parse_char(position, ':').rule("var").catch(&self.errors)?;
                    // ty=type
                    // type
                    let (position, _) = self._parse_skip(position)?;
                    let (position, v1) = self._parse_type(position).rule("var").catch(&self.errors)?;
                    let value = VarSeq { _0: v0.into(), ty: v1.into(), span: Span::default(), };
                    Ok((position, value))
                });
                seq_result
            });
            let (position, v2) = opt_result?;
            // '='
            let (position, _) = self._parse_skip(position)?;
            let (position, v3) = self.input.parse_char(position, '=').rule("var").catch(&self.errors)?;
            // init=expr
            // expr
            let (position, _) = self._parse_skip(position)?;
            let (position, v4) = self._parse_expr(position).rule("var").catch(&self.errors)?;
            // end
            let (position, _) = self._parse_skip(position)?;
            let (position, v5) = self._parse_end(position).rule("var").catch(&self.errors)?;
            let value = Var { _0: v0.into(), name: v1.into(), ty: v2.into(), _1: v3.into(), init: v4.into(), _2: v5.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_expr(&mut self) -> Result<Expr<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_expr(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_expr(&self, position: Position) -> ParseResult<Expr<'i>> {
        // (value=@ops((left=@right op=("=" | "+=" | "-=" | "*=" | "/=" | "%=") right=@self) | (left=@self op=("||" | "or") right=@self) | (left=@self op=("&&" | "and") right=@self) | (left=@self op=("==" | "!=") right=@self) | (left=@self op=(">=" | "<=" | "<" | ">") right=@self) | (left=@self op=("+" | "-") right=@self) | (left=@self op=("*" | "/" | "%") right=@self) | prefix))
        let seq_result = node(position, |position| {
            // value=@ops((left=@right op=("=" | "+=" | "-=" | "*=" | "/=" | "%=") right=@self) | (left=@self op=("||" | "or") right=@self) | (left=@self op=("&&" | "and") right=@self) | (left=@self op=("==" | "!=") right=@self) | (left=@self op=(">=" | "<=" | "<" | ">") right=@self) | (left=@self op=("+" | "-") right=@self) | (left=@self op=("*" | "/" | "%") right=@self) | prefix)
            // @ops((left=@right op=("=" | "+=" | "-=" | "*=" | "/=" | "%=") right=@self) | (left=@self op=("||" | "or") right=@self) | (left=@self op=("&&" | "and") right=@self) | (left=@self op=("==" | "!=") right=@self) | (left=@self op=(">=" | "<=" | "<" | ">") right=@self) | (left=@self op=("+" | "-") right=@self) | (left=@self op=("*" | "/" | "%") right=@self) | prefix)
            let ops_result = 
                Op::new(|position| {
                    // prefix
                    let (position, _) = self._parse_skip(position)?;
                    self._parse_prefix(position).rule("expr").catch(&self.errors).cast::<ExprAlt>()
                })
                .left_assoc(|position, v0, parse| {
                    let seq_result = node(position, |position| {
                        // op=("*" | "/" | "%")
                        // ("*" | "/" | "%")
                        let alt_result = 
                            (|| {
                                // "*"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "*").rule("expr").catch(&self.errors)
                            })()
                            .alt(|| {
                                // "/"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "/").rule("expr").catch(&self.errors)
                            })
                            .alt(|| {
                                // "%"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "%").rule("expr").catch(&self.errors)
                            })
                        ;
                        let (position, v1) = alt_result?;
                        // right=@self
                        // @self
                        let (position, _) = self._parse_skip(position)?;
                        let (position, v2) = parse(position).rule("expr").catch(&self.errors)?;
                        let value = ExprAltSeq { left: v0.into(), op: v1.into(), right: v2.into(), span: Span::default(), };
                        Ok((position, value))
                    });
                    seq_result.cast::<ExprAlt>()
                })
                .left_assoc(|position, v0, parse| {
                    let seq_result = node(position, |position| {
                        // op=("+" | "-")
                        // ("+" | "-")
                        let alt_result = 
                            (|| {
                                // "+"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "+").rule("expr").catch(&self.errors)
                            })()
                            .alt(|| {
                                // "-"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "-").rule("expr").catch(&self.errors)
                            })
                        ;
                        let (position, v1) = alt_result?;
                        // right=@self
                        // @self
                        let (position, _) = self._parse_skip(position)?;
                        let (position, v2) = parse(position).rule("expr").catch(&self.errors)?;
                        let value = ExprAltSeq { left: v0.into(), op: v1.into(), right: v2.into(), span: Span::default(), };
                        Ok((position, value))
                    });
                    seq_result.cast::<ExprAlt>()
                })
                .left_assoc(|position, v0, parse| {
                    let seq_result = node(position, |position| {
                        // op=(">=" | "<=" | "<" | ">")
                        // (">=" | "<=" | "<" | ">")
                        let alt_result = 
                            (|| {
                                // ">="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, ">=").rule("expr").catch(&self.errors)
                            })()
                            .alt(|| {
                                // "<="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "<=").rule("expr").catch(&self.errors)
                            })
                            .alt(|| {
                                // "<"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "<").rule("expr").catch(&self.errors)
                            })
                            .alt(|| {
                                // ">"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, ">").rule("expr").catch(&self.errors)
                            })
                        ;
                        let (position, v1) = alt_result?;
                        // right=@self
                        // @self
                        let (position, _) = self._parse_skip(position)?;
                        let (position, v2) = parse(position).rule("expr").catch(&self.errors)?;
                        let value = ExprAltSeq { left: v0.into(), op: v1.into(), right: v2.into(), span: Span::default(), };
                        Ok((position, value))
                    });
                    seq_result.cast::<ExprAlt>()
                })
                .left_assoc(|position, v0, parse| {
                    let seq_result = node(position, |position| {
                        // op=("==" | "!=")
                        // ("==" | "!=")
                        let alt_result = 
                            (|| {
                                // "=="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "==").rule("expr").catch(&self.errors)
                            })()
                            .alt(|| {
                                // "!="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "!=").rule("expr").catch(&self.errors)
                            })
                        ;
                        let (position, v1) = alt_result?;
                        // right=@self
                        // @self
                        let (position, _) = self._parse_skip(position)?;
                        let (position, v2) = parse(position).rule("expr").catch(&self.errors)?;
                        let value = ExprAltSeq { left: v0.into(), op: v1.into(), right: v2.into(), span: Span::default(), };
                        Ok((position, value))
                    });
                    seq_result.cast::<ExprAlt>()
                })
                .left_assoc(|position, v0, parse| {
                    let seq_result = node(position, |position| {
                        // op=("&&" | "and")
                        // ("&&" | "and")
                        let alt_result = 
                            (|| {
                                // "&&"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "&&").rule("expr").catch(&self.errors)
                            })()
                            .alt(|| {
                                // "and"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "and").rule("expr").catch(&self.errors)
                            })
                        ;
                        let (position, v1) = alt_result?;
                        // right=@self
                        // @self
                        let (position, _) = self._parse_skip(position)?;
                        let (position, v2) = parse(position).rule("expr").catch(&self.errors)?;
                        let value = ExprAltSeq { left: v0.into(), op: v1.into(), right: v2.into(), span: Span::default(), };
                        Ok((position, value))
                    });
                    seq_result.cast::<ExprAlt>()
                })
                .left_assoc(|position, v0, parse| {
                    let seq_result = node(position, |position| {
                        // op=("||" | "or")
                        // ("||" | "or")
                        let alt_result = 
                            (|| {
                                // "||"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "||").rule("expr").catch(&self.errors)
                            })()
                            .alt(|| {
                                // "or"
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "or").rule("expr").catch(&self.errors)
                            })
                        ;
                        let (position, v1) = alt_result?;
                        // right=@self
                        // @self
                        let (position, _) = self._parse_skip(position)?;
                        let (position, v2) = parse(position).rule("expr").catch(&self.errors)?;
                        let value = ExprAltSeq { left: v0.into(), op: v1.into(), right: v2.into(), span: Span::default(), };
                        Ok((position, value))
                    });
                    seq_result.cast::<ExprAlt>()
                })
                .right_assoc(|position, v0, parse| {
                    let seq_result = node(position, |position| {
                        // op=("=" | "+=" | "-=" | "*=" | "/=" | "%=")
                        // ("=" | "+=" | "-=" | "*=" | "/=" | "%=")
                        let alt_result = 
                            (|| {
                                // "="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "=").rule("expr").catch(&self.errors)
                            })()
                            .alt(|| {
                                // "+="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "+=").rule("expr").catch(&self.errors)
                            })
                            .alt(|| {
                                // "-="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "-=").rule("expr").catch(&self.errors)
                            })
                            .alt(|| {
                                // "*="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "*=").rule("expr").catch(&self.errors)
                            })
                            .alt(|| {
                                // "/="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "/=").rule("expr").catch(&self.errors)
                            })
                            .alt(|| {
                                // "%="
                                let (position, _) = self._parse_skip(position)?;
                                self.input.parse_str(position, "%=").rule("expr").catch(&self.errors)
                            })
                        ;
                        let (position, v1) = alt_result?;
                        // right=@self
                        // @self
                        let (position, _) = self._parse_skip(position)?;
                        let (position, v2) = parse(position).rule("expr").catch(&self.errors)?;
                        let value = ExprAltSeq { left: v0.into(), op: v1.into(), right: v2.into(), span: Span::default(), };
                        Ok((position, value))
                    });
                    seq_result.cast::<ExprAlt>()
                })
            .call(position);
            let (position, v0) = ops_result?;
            let value = Expr { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_prefix(&mut self) -> Result<Prefix<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_prefix(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_prefix(&self, position: Position) -> ParseResult<Prefix<'i>> {
        // (ops=('-' | '+' | not_op)* postfix)
        let seq_result = node(position, |position| {
            // ops=('-' | '+' | not_op)*
            // ('-' | '+' | not_op)*
            let many_result = many(position, |position| {
                // ('-' | '+' | not_op)
                let alt_result = 
                    (|| {
                        // '-'
                        let (position, _) = self._parse_skip(position)?;
                        self.input.parse_char(position, '-').rule("prefix").catch(&self.errors).cast::<PrefixAlt>()
                    })()
                    .alt(|| {
                        // '+'
                        let (position, _) = self._parse_skip(position)?;
                        self.input.parse_char(position, '+').rule("prefix").catch(&self.errors).cast::<PrefixAlt>()
                    })
                    .alt(|| {
                        // not_op
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_not_op(position).rule("prefix").catch(&self.errors).cast::<PrefixAlt>()
                    })
                ;
                alt_result
            });
            let (position, v0) = many_result?;
            // postfix
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_postfix(position).rule("prefix").catch(&self.errors)?;
            let value = Prefix { ops: v0.into(), _0: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_not_op(&mut self) -> Result<NotOp<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_not_op(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_not_op(&self, position: Position) -> ParseResult<NotOp<'i>> {
        // (str=$(('!' | "not")))
        let seq_result = node(position, |position| {
            // str=$(('!' | "not"))
            // $(('!' | "not"))
            let as_str_result = self.input.as_str(position, |position| {
                // ('!' | "not")
                let alt_result = 
                    (|| {
                        // '!'
                        self.input.parse_char(position, '!').rule("not_op").catch(&self.errors).unit()
                    })()
                    .alt(|| {
                        // "not"
                        self.input.parse_str(position, "not").rule("not_op").catch(&self.errors).unit()
                    })
                ;
                alt_result
            });
            let (position, v0) = as_str_result?;
            let value = NotOp { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_postfix(&mut self) -> Result<Postfix<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_postfix(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_postfix(&self, position: Position) -> ParseResult<Postfix<'i>> {
        // (expr=atom ops=(dot_access | array_access | call)*)
        let seq_result = node(position, |position| {
            // expr=atom
            // atom
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_atom(position).rule("postfix").catch(&self.errors)?;
            // ops=(dot_access | array_access | call)*
            // (dot_access | array_access | call)*
            let many_result = many(position, |position| {
                // (dot_access | array_access | call)
                let alt_result = 
                    (|| {
                        // dot_access
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_dot_access(position).rule("postfix").catch(&self.errors).cast::<PostfixAlt>()
                    })()
                    .alt(|| {
                        // array_access
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_array_access(position).rule("postfix").catch(&self.errors).cast::<PostfixAlt>()
                    })
                    .alt(|| {
                        // call
                        let (position, _) = self._parse_skip(position)?;
                        self._parse_call(position).rule("postfix").catch(&self.errors).cast::<PostfixAlt>()
                    })
                ;
                alt_result
            });
            let (position, v1) = many_result?;
            let value = Postfix { expr: v0.into(), ops: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_dot_access(&mut self) -> Result<DotAccess<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_dot_access(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_dot_access(&self, position: Position) -> ParseResult<DotAccess<'i>> {
        // ('.' field=ident)
        let seq_result = node(position, |position| {
            // '.'
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '.').rule("dot_access").catch(&self.errors)?;
            // field=ident
            // ident
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_ident(position).rule("dot_access").catch(&self.errors)?;
            let value = DotAccess { _0: v0.into(), field: v1.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_array_access(&mut self) -> Result<ArrayAccess<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_array_access(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_array_access(&self, position: Position) -> ParseResult<ArrayAccess<'i>> {
        // ('[' index=expr ']')
        let seq_result = node(position, |position| {
            // '['
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '[').rule("array_access").catch(&self.errors)?;
            // index=expr
            // expr
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_expr(position).rule("array_access").catch(&self.errors)?;
            // ']'
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, ']').rule("array_access").catch(&self.errors)?;
            let value = ArrayAccess { _0: v0.into(), index: v1.into(), _1: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_call(&mut self) -> Result<Call<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_call(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_call(&self, position: Position) -> ParseResult<Call<'i>> {
        // ('(' args=expr_args? ')')
        let seq_result = node(position, |position| {
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '(').rule("call").catch(&self.errors)?;
            // args=expr_args?
            // expr_args?
            let opt_result = opt(position, |position| {
                // expr_args
                let (position, _) = self._parse_skip(position)?;
                self._parse_expr_args(position).rule("call").catch(&self.errors)
            });
            let (position, v1) = opt_result?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, ')').rule("call").catch(&self.errors)?;
            let value = Call { _0: v0.into(), args: v1.into(), _1: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_expr_args(&mut self) -> Result<ExprArgs<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_expr_args(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_expr_args(&self, position: Position) -> ParseResult<ExprArgs<'i>> {
        // (first=expr rest=(',' arg=expr) ','?)
        let seq_result = node(position, |position| {
            // first=expr
            // expr
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self._parse_expr(position).rule("expr_args").catch(&self.errors)?;
            // rest=(',' arg=expr)
            // (',' arg=expr)
            let seq_result = node(position, |position| {
                // ','
                let (position, _) = self._parse_skip(position)?;
                let (position, v0) = self.input.parse_char(position, ',').rule("expr_args").catch(&self.errors)?;
                // arg=expr
                // expr
                let (position, _) = self._parse_skip(position)?;
                let (position, v1) = self._parse_expr(position).rule("expr_args").catch(&self.errors)?;
                let value = ExprArgsSeq { _0: v0.into(), arg: v1.into(), span: Span::default(), };
                Ok((position, value))
            });
            let (position, v1) = seq_result?;
            // ','?
            let opt_result = opt(position, |position| {
                // ','
                let (position, _) = self._parse_skip(position)?;
                self.input.parse_char(position, ',').rule("expr_args").catch(&self.errors)
            });
            let (position, v2) = opt_result?;
            let value = ExprArgs { first: v0.into(), rest: v1.into(), _0: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_atom(&mut self) -> Result<Atom<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_atom(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_atom(&self, position: Position) -> ParseResult<Atom<'i>> {
        // (value=(float | uint | false | true | null | ident | sub_expr))
        let seq_result = node(position, |position| {
            // value=(float | uint | false | true | null | ident | sub_expr)
            // (float | uint | false | true | null | ident | sub_expr)
            let alt_result = 
                (|| {
                    // float
                    self._parse_float(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })()
                .alt(|| {
                    // uint
                    self._parse_uint(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // false
                    self._parse_false(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // true
                    self._parse_true(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // null
                    self._parse_null(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // ident
                    self._parse_ident(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
                .alt(|| {
                    // sub_expr
                    self._parse_sub_expr(position).rule("atom").catch(&self.errors).cast::<AtomAlt>()
                })
            ;
            let (position, v0) = alt_result?;
            let value = Atom { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_false(&mut self) -> Result<False<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_false(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_false(&self, position: Position) -> ParseResult<False<'i>> {
        // (value="false")
        let seq_result = node(position, |position| {
            // value="false"
            // "false"
            let (position, v0) = self.input.parse_str(position, "false").rule("false").catch(&self.errors)?;
            let value = False { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_true(&mut self) -> Result<True<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_true(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_true(&self, position: Position) -> ParseResult<True<'i>> {
        // (value="true")
        let seq_result = node(position, |position| {
            // value="true"
            // "true"
            let (position, v0) = self.input.parse_str(position, "true").rule("true").catch(&self.errors)?;
            let value = True { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_null(&mut self) -> Result<Null<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_null(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_null(&self, position: Position) -> ParseResult<Null<'i>> {
        // (value="null")
        let seq_result = node(position, |position| {
            // value="null"
            // "null"
            let (position, v0) = self.input.parse_str(position, "null").rule("null").catch(&self.errors)?;
            let value = Null { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_sub_expr(&mut self) -> Result<SubExpr<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_sub_expr(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_sub_expr(&self, position: Position) -> ParseResult<SubExpr<'i>> {
        // ('(' expr=expr ')')
        let seq_result = node(position, |position| {
            // '('
            let (position, _) = self._parse_skip(position)?;
            let (position, v0) = self.input.parse_char(position, '(').rule("sub_expr").catch(&self.errors)?;
            // expr=expr
            // expr
            let (position, _) = self._parse_skip(position)?;
            let (position, v1) = self._parse_expr(position).rule("sub_expr").catch(&self.errors)?;
            // ')'
            let (position, _) = self._parse_skip(position)?;
            let (position, v2) = self.input.parse_char(position, ')').rule("sub_expr").catch(&self.errors)?;
            let value = SubExpr { _0: v0.into(), expr: v1.into(), _1: v2.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_end(&mut self) -> Result<End<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_end(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_end(&self, position: Position) -> ParseResult<End<'i>> {
        // (str=$(';'+))
        let seq_result = node(position, |position| {
            // str=$(';'+)
            // $(';'+)
            let as_str_result = self.input.as_str(position, |position| {
                // ';'+
                let some_result = some(position, |position| {
                    // ';'
                    self.input.parse_char(position, ';').rule("end").catch(&self.errors)
                });
                some_result
            });
            let (position, v0) = as_str_result?;
            let value = End { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ident(&mut self) -> Result<Ident<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_ident(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ident(&self, position: Position) -> ParseResult<Ident<'i>> {
        // (str=$((letter (letter | digit)*)))
        let seq_result = node(position, |position| {
            // str=$((letter (letter | digit)*))
            // $((letter (letter | digit)*))
            let as_str_result = self.input.as_str(position, |position| {
                // (letter (letter | digit)*)
                let seq_result = (|| {
                    // letter
                    let (position, _) = self._parse_letter(position).rule("ident").catch(&self.errors)?;
                    // (letter | digit)*
                    let many_result = many(position, |position| {
                        // (letter | digit)
                        let alt_result = 
                            (|| {
                                // letter
                                self._parse_letter(position).rule("ident").catch(&self.errors).unit()
                            })()
                            .alt(|| {
                                // digit
                                self._parse_digit(position).rule("ident").catch(&self.errors).unit()
                            })
                        ;
                        alt_result
                    });
                    let (position, _) = many_result?;
                    Ok((position, ()))
                })();
                seq_result
            });
            let (position, v0) = as_str_result?;
            let value = Ident { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_uint(&mut self) -> Result<Uint<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_uint(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_uint(&self, position: Position) -> ParseResult<Uint<'i>> {
        // (str=$(digit+))
        let seq_result = node(position, |position| {
            // str=$(digit+)
            // $(digit+)
            let as_str_result = self.input.as_str(position, |position| {
                // digit+
                let some_result = some(position, |position| {
                    // digit
                    self._parse_digit(position).rule("uint").catch(&self.errors)
                });
                some_result
            });
            let (position, v0) = as_str_result?;
            let value = Uint { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_float(&mut self) -> Result<Float<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_float(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_float(&self, position: Position) -> ParseResult<Float<'i>> {
        // (str=$((uint '.' uint)))
        let seq_result = node(position, |position| {
            // str=$((uint '.' uint))
            // $((uint '.' uint))
            let as_str_result = self.input.as_str(position, |position| {
                // (uint '.' uint)
                let seq_result = (|| {
                    // uint
                    let (position, _) = self._parse_uint(position).rule("float").catch(&self.errors)?;
                    // '.'
                    let (position, _) = self.input.parse_char(position, '.').rule("float").catch(&self.errors)?;
                    // uint
                    let (position, _) = self._parse_uint(position).rule("float").catch(&self.errors)?;
                    Ok((position, ()))
                })();
                seq_result
            });
            let (position, v0) = as_str_result?;
            let value = Float { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_letter(&mut self) -> Result<Letter, Error> {
        let position = self.position;
        let (position, result) = self._parse_letter(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_letter(&self, position: Position) -> ParseResult<Letter> {
        // (value=('a'..'z' | 'A'..'Z' | '_'))
        let seq_result = node(position, |position| {
            // value=('a'..'z' | 'A'..'Z' | '_')
            // ('a'..'z' | 'A'..'Z' | '_')
            let alt_result = 
                (|| {
                    // 'a'..'z'
                    self.input.parse_char_range(position, 'a', 'z').rule("letter").catch(&self.errors)
                })()
                .alt(|| {
                    // 'A'..'Z'
                    self.input.parse_char_range(position, 'A', 'Z').rule("letter").catch(&self.errors)
                })
                .alt(|| {
                    // '_'
                    self.input.parse_char(position, '_').rule("letter").catch(&self.errors)
                })
            ;
            let (position, v0) = alt_result?;
            let value = Letter { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_digit(&mut self) -> Result<Digit, Error> {
        let position = self.position;
        let (position, result) = self._parse_digit(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_digit(&self, position: Position) -> ParseResult<Digit> {
        // (value='0'..'9')
        let seq_result = node(position, |position| {
            // value='0'..'9'
            // '0'..'9'
            let (position, v0) = self.input.parse_char_range(position, '0', '9').rule("digit").catch(&self.errors)?;
            let value = Digit { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_ws(&mut self) -> Result<Ws, Error> {
        let position = self.position;
        let (position, result) = self._parse_ws(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_ws(&self, position: Position) -> ParseResult<Ws> {
        // (value=(' ' | '\r' | '\t' | '\n'))
        let seq_result = node(position, |position| {
            // value=(' ' | '\r' | '\t' | '\n')
            // (' ' | '\r' | '\t' | '\n')
            let alt_result = 
                (|| {
                    // ' '
                    self.input.parse_char(position, ' ').rule("ws").catch(&self.errors)
                })()
                .alt(|| {
                    // '\r'
                    self.input.parse_char(position, '\r').rule("ws").catch(&self.errors)
                })
                .alt(|| {
                    // '\t'
                    self.input.parse_char(position, '\t').rule("ws").catch(&self.errors)
                })
                .alt(|| {
                    // '\n'
                    self.input.parse_char(position, '\n').rule("ws").catch(&self.errors)
                })
            ;
            let (position, v0) = alt_result?;
            let value = Ws { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_comment(&mut self) -> Result<Comment<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_comment(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_comment(&self, position: Position) -> ParseResult<Comment<'i>> {
        // (str=$(('#' ANY ~ '\n'* '\n')))
        let seq_result = node(position, |position| {
            // str=$(('#' ANY ~ '\n'* '\n'))
            // $(('#' ANY ~ '\n'* '\n'))
            let as_str_result = self.input.as_str(position, |position| {
                // ('#' ANY ~ '\n'* '\n')
                let seq_result = (|| {
                    // '#'
                    let (position, _) = self.input.parse_char(position, '#').rule("comment").catch(&self.errors)?;
                    // ANY ~ '\n'*
                    let many_result = many(position, |position| {
                        // ANY ~ '\n'
                        let and_not_result = and_not(position, |position| {
                            // ANY
                            self.input.next(position).rule("comment").catch(&self.errors)
                            }, |position| {
                            // '\n'
                            self.input.parse_char(position, '\n').rule("comment").catch(&self.errors)
                        });
                        and_not_result
                    });
                    let (position, _) = many_result?;
                    // '\n'
                    let (position, _) = self.input.parse_char(position, '\n').rule("comment").catch(&self.errors)?;
                    Ok((position, ()))
                })();
                seq_result
            });
            let (position, v0) = as_str_result?;
            let value = Comment { str: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        rule_result
    }
    
    pub fn parse_skip(&mut self) -> Result<Skip<'i>, Error> {
        let position = self.position;
        let (position, result) = self._parse_skip(position)?;
        self.position = position;
        Ok(result)
    }
    fn _parse_skip(&self, position: Position) -> ParseResult<Skip<'i>> {
        self.errors.stop();
        // (value=(ws | comment)*)
        let seq_result = node(position, |position| {
            // value=(ws | comment)*
            // (ws | comment)*
            let many_result = many(position, |position| {
                // (ws | comment)
                let alt_result = 
                    (|| {
                        // ws
                        self._parse_ws(position).rule("skip").catch(&self.errors).cast::<SkipAlt>()
                    })()
                    .alt(|| {
                        // comment
                        self._parse_comment(position).rule("skip").catch(&self.errors).cast::<SkipAlt>()
                    })
                ;
                alt_result
            });
            let (position, v0) = many_result?;
            let value = Skip { value: v0.into(), span: Span::default(), };
            Ok((position, value))
        });
        let rule_result = seq_result;
        self.errors.start();
        rule_result
    }
    
}

// cache
#[derive(Clone, Debug, Default)]
struct Caches<'i> {
    _d: std::marker::PhantomData<&'i ()>,
}
// impls

impl<'i> From<End<'i>> for DeclsAlt<'i> {
    fn from(v: End<'i>) -> Self {
        Self::End(v)
    }
}
impl<'i> From<Decl<'i>> for DeclsAlt<'i> {
    fn from(v: Decl<'i>) -> Self {
        Self::Decl(v)
    }
}
impl<'i> Node for DeclsAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::End(value) => value.set_span(span),
            Self::Decl(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::End(value) => value.get_span(),
            Self::Decl(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for DeclsAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::End(value) => value.format(fmt),
            Self::Decl(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Decls<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Decls<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Decls")?;
        fmt.write_named("decls", &self.decls)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Struct<'i>> for DeclAlt<'i> {
    fn from(v: Struct<'i>) -> Self {
        Self::Struct(v)
    }
}
impl<'i> From<Func<'i>> for DeclAlt<'i> {
    fn from(v: Func<'i>) -> Self {
        Self::Func(v)
    }
}
impl<'i> Node for DeclAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Struct(value) => value.set_span(span),
            Self::Func(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Struct(value) => value.get_span(),
            Self::Func(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for DeclAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Struct(value) => value.format(fmt),
            Self::Func(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Decl<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Decl<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Decl")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Struct<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Struct<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Struct")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("name", &self.name)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("fields", &self.fields)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_unnamed("_3", &self._3)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for FuncSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for FuncSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("FuncSeq")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("ty", &self.ty)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Func<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Func<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Func")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("name", &self.name)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("args", &self.args)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_named("return_type", &self.return_type)?;
        fmt.write_named("body", &self.body)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ArgsSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ArgsSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ArgsSeq")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("arg", &self.arg)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Args<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Args<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Args")?;
        fmt.write_named("first", &self.first)?;
        fmt.write_named("rest", &self.rest)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Arg<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Arg<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Arg")?;
        fmt.write_named("name", &self.name)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("ty", &self.ty)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<PtrType<'i>> for TypeAlt<'i> {
    fn from(v: PtrType<'i>) -> Self {
        Self::PtrType(v)
    }
}
impl<'i> From<ArrayType<'i>> for TypeAlt<'i> {
    fn from(v: ArrayType<'i>) -> Self {
        Self::ArrayType(v)
    }
}
impl<'i> From<Ident<'i>> for TypeAlt<'i> {
    fn from(v: Ident<'i>) -> Self {
        Self::Ident(v)
    }
}
impl<'i> Node for TypeAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::PtrType(value) => value.set_span(span),
            Self::ArrayType(value) => value.set_span(span),
            Self::Ident(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::PtrType(value) => value.get_span(),
            Self::ArrayType(value) => value.get_span(),
            Self::Ident(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for TypeAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::PtrType(value) => value.format(fmt),
            Self::ArrayType(value) => value.format(fmt),
            Self::Ident(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Type<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Type<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Type")?;
        fmt.write_named("ty", &self.ty)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for PtrType<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for PtrType<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("PtrType")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("ty", &self.ty)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ArrayType<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ArrayType<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ArrayType")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("ty", &self.ty)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_named("size", &self.size)?;
        fmt.write_unnamed("_3", &self._3)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<End<'i>> for BlockAlt<'i> {
    fn from(v: End<'i>) -> Self {
        Self::End(v)
    }
}
impl<'i> From<Stmt<'i>> for BlockAlt<'i> {
    fn from(v: Stmt<'i>) -> Self {
        Self::Stmt(v)
    }
}
impl<'i> Node for BlockAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::End(value) => value.set_span(span),
            Self::Stmt(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::End(value) => value.get_span(),
            Self::Stmt(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for BlockAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::End(value) => value.format(fmt),
            Self::Stmt(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Block<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Block<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Block")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("stmts", &self.stmts)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Return<'i>> for StmtAlt<'i> {
    fn from(v: Return<'i>) -> Self {
        Self::Return(v)
    }
}
impl<'i> From<While<'i>> for StmtAlt<'i> {
    fn from(v: While<'i>) -> Self {
        Self::While(v)
    }
}
impl<'i> From<Var<'i>> for StmtAlt<'i> {
    fn from(v: Var<'i>) -> Self {
        Self::Var(v)
    }
}
impl<'i> From<ExprStmt<'i>> for StmtAlt<'i> {
    fn from(v: ExprStmt<'i>) -> Self {
        Self::ExprStmt(v)
    }
}
impl<'i> Node for StmtAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Return(value) => value.set_span(span),
            Self::While(value) => value.set_span(span),
            Self::Var(value) => value.set_span(span),
            Self::ExprStmt(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Return(value) => value.get_span(),
            Self::While(value) => value.get_span(),
            Self::Var(value) => value.get_span(),
            Self::ExprStmt(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for StmtAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Return(value) => value.format(fmt),
            Self::While(value) => value.format(fmt),
            Self::Var(value) => value.format(fmt),
            Self::ExprStmt(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Stmt<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Stmt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Stmt")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Return<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Return<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Return")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("result", &self.result)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for While<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for While<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("While")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("cond", &self.cond)?;
        fmt.write_named("body", &self.body)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ExprStmt<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ExprStmt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ExprStmt")?;
        fmt.write_named("expr", &self.expr)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for VarSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for VarSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("VarSeq")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("ty", &self.ty)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Var<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Var<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Var")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("name", &self.name)?;
        fmt.write_named("ty", &self.ty)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_named("init", &self.init)?;
        fmt.write_unnamed("_2", &self._2)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ExprAltSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ExprAltSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ExprAltSeq")?;
        fmt.write_named("left", &self.left)?;
        fmt.write_named("op", &self.op)?;
        fmt.write_named("right", &self.right)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<ExprAltSeq<'i>> for ExprAlt<'i> {
    fn from(v: ExprAltSeq<'i>) -> Self {
        Self::ExprAltSeq(v)
    }
}
impl<'i> From<Prefix<'i>> for ExprAlt<'i> {
    fn from(v: Prefix<'i>) -> Self {
        Self::Prefix(v)
    }
}
impl<'i> Node for ExprAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::ExprAltSeq(value) => value.set_span(span),
            Self::Prefix(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::ExprAltSeq(value) => value.get_span(),
            Self::Prefix(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for ExprAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::ExprAltSeq(value) => value.format(fmt),
            Self::Prefix(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Expr<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Expr<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Expr")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<char> for PrefixAlt<'i> {
    fn from(v: char) -> Self {
        Self::Char(v)
    }
}
impl<'i> From<NotOp<'i>> for PrefixAlt<'i> {
    fn from(v: NotOp<'i>) -> Self {
        Self::NotOp(v)
    }
}
impl<'i> Node for PrefixAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Char(_) => {}
            Self::NotOp(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Char(_) => Span::default(),
            Self::NotOp(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for PrefixAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Char(value) => value.format(fmt),
            Self::NotOp(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Prefix<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Prefix<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Prefix")?;
        fmt.write_named("ops", &self.ops)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for NotOp<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for NotOp<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("NotOp")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<DotAccess<'i>> for PostfixAlt<'i> {
    fn from(v: DotAccess<'i>) -> Self {
        Self::DotAccess(v)
    }
}
impl<'i> From<ArrayAccess<'i>> for PostfixAlt<'i> {
    fn from(v: ArrayAccess<'i>) -> Self {
        Self::ArrayAccess(v)
    }
}
impl<'i> From<Call<'i>> for PostfixAlt<'i> {
    fn from(v: Call<'i>) -> Self {
        Self::Call(v)
    }
}
impl<'i> Node for PostfixAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::DotAccess(value) => value.set_span(span),
            Self::ArrayAccess(value) => value.set_span(span),
            Self::Call(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::DotAccess(value) => value.get_span(),
            Self::ArrayAccess(value) => value.get_span(),
            Self::Call(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for PostfixAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::DotAccess(value) => value.format(fmt),
            Self::ArrayAccess(value) => value.format(fmt),
            Self::Call(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Postfix<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Postfix<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Postfix")?;
        fmt.write_named("expr", &self.expr)?;
        fmt.write_named("ops", &self.ops)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for DotAccess<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for DotAccess<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("DotAccess")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("field", &self.field)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ArrayAccess<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ArrayAccess<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ArrayAccess")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("index", &self.index)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Call<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Call<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Call")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("args", &self.args)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ExprArgsSeq<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ExprArgsSeq<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ExprArgsSeq")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("arg", &self.arg)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for ExprArgs<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for ExprArgs<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("ExprArgs")?;
        fmt.write_named("first", &self.first)?;
        fmt.write_named("rest", &self.rest)?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Float<'i>> for AtomAlt<'i> {
    fn from(v: Float<'i>) -> Self {
        Self::Float(v)
    }
}
impl<'i> From<Uint<'i>> for AtomAlt<'i> {
    fn from(v: Uint<'i>) -> Self {
        Self::Uint(v)
    }
}
impl<'i> From<False<'i>> for AtomAlt<'i> {
    fn from(v: False<'i>) -> Self {
        Self::False(v)
    }
}
impl<'i> From<True<'i>> for AtomAlt<'i> {
    fn from(v: True<'i>) -> Self {
        Self::True(v)
    }
}
impl<'i> From<Null<'i>> for AtomAlt<'i> {
    fn from(v: Null<'i>) -> Self {
        Self::Null(v)
    }
}
impl<'i> From<Ident<'i>> for AtomAlt<'i> {
    fn from(v: Ident<'i>) -> Self {
        Self::Ident(v)
    }
}
impl<'i> From<SubExpr<'i>> for AtomAlt<'i> {
    fn from(v: SubExpr<'i>) -> Self {
        Self::SubExpr(v)
    }
}
impl<'i> Node for AtomAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Float(value) => value.set_span(span),
            Self::Uint(value) => value.set_span(span),
            Self::False(value) => value.set_span(span),
            Self::True(value) => value.set_span(span),
            Self::Null(value) => value.set_span(span),
            Self::Ident(value) => value.set_span(span),
            Self::SubExpr(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Float(value) => value.get_span(),
            Self::Uint(value) => value.get_span(),
            Self::False(value) => value.get_span(),
            Self::True(value) => value.get_span(),
            Self::Null(value) => value.get_span(),
            Self::Ident(value) => value.get_span(),
            Self::SubExpr(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for AtomAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Float(value) => value.format(fmt),
            Self::Uint(value) => value.format(fmt),
            Self::False(value) => value.format(fmt),
            Self::True(value) => value.format(fmt),
            Self::Null(value) => value.format(fmt),
            Self::Ident(value) => value.format(fmt),
            Self::SubExpr(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Atom<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Atom<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Atom")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for False<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for False<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("False")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for True<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for True<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("True")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Null<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Null<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Null")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for SubExpr<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for SubExpr<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("SubExpr")?;
        fmt.write_unnamed("_0", &self._0)?;
        fmt.write_named("expr", &self.expr)?;
        fmt.write_unnamed("_1", &self._1)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for End<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for End<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("End")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Ident<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Ident<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Ident")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Uint<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Uint<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Uint")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Float<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Float<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Float")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl Node for Letter {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl TreeDisplay for Letter {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Letter")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl Node for Digit {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl TreeDisplay for Digit {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Digit")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl Node for Ws {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl TreeDisplay for Ws {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Ws")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> Node for Comment<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Comment<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Comment")?;
        fmt.write_named("str", &self.str)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}
impl<'i> From<Ws> for SkipAlt<'i> {
    fn from(v: Ws) -> Self {
        Self::Ws(v)
    }
}
impl<'i> From<Comment<'i>> for SkipAlt<'i> {
    fn from(v: Comment<'i>) -> Self {
        Self::Comment(v)
    }
}
impl<'i> Node for SkipAlt<'i> {
    fn set_span(&mut self, span: Span) {
        match self {
            Self::Ws(value) => value.set_span(span),
            Self::Comment(value) => value.set_span(span),
        }
    }
    fn get_span(&self) -> Span {
        match self {
            Self::Ws(value) => value.get_span(),
            Self::Comment(value) => value.get_span(),
        }
    }
}
impl<'i> TreeDisplay for SkipAlt<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::Ws(value) => value.format(fmt),
            Self::Comment(value) => value.format(fmt),
        }
    }
}
impl<'i> Node for Skip<'i> {
    fn set_span(&mut self, span: Span) {
        self.span = span;
    }
    fn get_span(&self) -> Span {
        self.span
    }
}
impl<'i> TreeDisplay for Skip<'i> {
    fn format(&self, fmt: &mut Formatter) -> std::fmt::Result {
        fmt.start_type("Skip")?;
        fmt.write_named("value", &self.value)?;
        fmt.write_span(self.span)?;
        fmt.end_type()
    }
}