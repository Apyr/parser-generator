mod parser;
use parser::*;
use parsergen_runtime::Config;

fn main() {
    let code = r#"
    struct Pair(a: Int, b: Int);

    fn main(): Int {
        var x: Int = 5;
        var y = 42;
        var r = x * y;
        r += x + y;
        return r;
    }
    "#;
    let parsed = Parser::new(code).parse_decls().unwrap();
    let config = Config {
        show_span: false,
        ..Default::default()
    };
    println!("{}", config.display(parsed));
}
